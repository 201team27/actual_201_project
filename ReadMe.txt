Jeffrey Vaudrin-McLean vaudrinm@usc.edu 30389
Christina Liang liangchr@usc.edu 29909
Rena Chen renachen@usc.edu 29994
Jessica Koe koe@usc.edu 30389
Clara Hong clarahon@usc.edu 30389

To configure:
After unzipping the project,
go to resources and run CreateAnythingGoes.sql in MySQL to create the database.
In networking package, run the FroggerServer.
Then in gameclient package, run GameClientWindow.

We have a website (outside technology):
scf.usc.edu/~renachen/FroggersRevenge
