CREATE DATABASE AnythingGoes;

USE AnythingGoes;

CREATE TABLE Player (
	playerID int(11) not null primary key auto_increment,
	username VARCHAR(50) not null,
    topScore int(11) not null,
    numEaten int(11) not null,
    bestTime VARCHAR(10) not null,
    worstTime varchar(10) not null,
    gamesPlayed int(11) not null,
    gamesWon int(11) not null,
    pass int(11) not null
);


