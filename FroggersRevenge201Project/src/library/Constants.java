package library;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Constants {
	// Strings associated with cards in overall gui layout (cardlayout)
	public static final String TITLEPANEL = "TITLE"; 
	public static final String LOGINPANEL = "LOGIN"; 
	public static final String LOBBYPANEL = "LOBBY"; 
	public static final String GAMEPANEL = "GAME";
	
	public static final ImageIcon CLOUDIMG = new ImageIcon("assets/images/screens/cloudbg.png");
	
	// IMAGES FOR TITLEPANEL
	public static final ImageIcon TITLEBUTTONIMG1 = new ImageIcon("assets/images/buttons/start_unpressed.png");
	public static final ImageIcon TITLEBUTTONIMG2 = new ImageIcon("assets/images/buttons/start_pressed.png");
	public static final ImageIcon TITLEBACKGROUND = new ImageIcon("assets/images/screens/title_screen.png");
	
	// IMAGES FOR LOGINPANEL
	public static final ImageIcon PLEASELOGINIMG = new ImageIcon("assets/images/screens/please_login.png");
	public static final ImageIcon LOGINPASSIMG = new ImageIcon("assets/images/screens/login_password.png");
	public static final ImageIcon LOGINBUTTONIMG1 = new ImageIcon("assets/images/buttons/login_unpressed.png");
	public static final ImageIcon LOGINBUTTONIMG2 = new ImageIcon("assets/images/buttons/login_pressed.png");
	public static final ImageIcon REGISTERBUTTONIMG1 = new ImageIcon("assets/images/buttons/register_unpressed.png");
	public static final ImageIcon REGISTERBUTTONIMG2 = new ImageIcon("assets/images/buttons/register_pressed.png");
	public static final ImageIcon DONTHAVEACCOUNTIMG = new ImageIcon("assets/images/screens/dont_have_account.png");
	public static final ImageIcon PLAYASGUESTIMG = new ImageIcon("assets/images/screens/play_as_guest.png");
	public static final ImageIcon GUESTSTARTBUTTONIMG1 = new ImageIcon("assets/images/buttons/guestStart_unpressed.png");
	public static final ImageIcon GUESTSTARTBUTTONIMG2 = new ImageIcon("assets/images/buttons/guestStart_pressed.png");
	public static final ImageIcon CREATEBUTTONIMG1 = new ImageIcon("assets/images/buttons/create_unpressed.png");
	public static final ImageIcon CREATEBUTTONIMG2 = new ImageIcon("assets/images/buttons/create_pressed.png");
	public static final ImageIcon DIVIDERIMG = new ImageIcon("assets/images/screens/divider.png");
	public static final ImageIcon THREECHARMESSAGEIMG = new ImageIcon("assets/images/screens/3charuser.png");
	public static final ImageIcon SIXCHARPASSIMG = new ImageIcon("assets/images/screens/6charpass.png");
	public static final ImageIcon NOSTARTGUESTIMG = new ImageIcon("assets/images/screens/nostartguest.png");
	public static final ImageIcon FLUFFYCLOUDIMG = new ImageIcon("assets/images/screens/fluffyclouds.png");
	public static final ImageIcon INVALIDIMG = new ImageIcon("assets/images/screens/invalid.png");
	public static final ImageIcon USERNAMECANIMG = new ImageIcon("assets/images/screens/usernamecan.png");
	public static final ImageIcon WHITESPACEIMG = new ImageIcon("assets/images/screens/whitespace.png");
	//these have yet to be implemented 
	public static final ImageIcon SUCCESSIMG = new ImageIcon("assets/images/screens/success.png");
	public static final ImageIcon TAKENIMG = new ImageIcon("assets/images/screens/taken.png");
	public static final ImageIcon ALREADYINIMG = new ImageIcon("assets/images/screens/alreadyin.png");
	public static final ImageIcon NOTEXISTIMG = new ImageIcon("assets/images/screens/notexist.png");
	public static final ImageIcon NOTMATCHIMG = new ImageIcon("assets/images/screens/notmatch.png");
	
	// IMAGES FOR LOBBYPANEL
	public static final ImageIcon VIEWPROFILEIMG1 = new ImageIcon("assets/images/buttons/viewProfile_unpressed.png");
	public static final ImageIcon VIEWPROFILEIMG2 = new ImageIcon("assets/images/buttons/viewProfile_pressed.png");
	public static final ImageIcon CHALLENGEBUTTONIMG1 = new ImageIcon("assets/images/buttons/challenge_unpressed.png");
	public static final ImageIcon CHALLENGEBUTTONIMG2 = new ImageIcon("assets/images/buttons/challenge_pressed.png");
	
	// IMAGES FOR GAMEBOARD 
	public static final ImageIcon BASICLILYIMG = new ImageIcon("assets/images/gamegui/basiclily.png");
	public static final ImageIcon COOKIEIMG = new ImageIcon("assets/images/gamegui/cookie.png");
	public static final ImageIcon CANDYIMG = new ImageIcon("assets/images/gamegui/candy.png");
	public static final ImageIcon CUPCAKEIMG = new ImageIcon("assets/images/gamegui/cupcake.png");
	public static final ImageIcon FIVEHOURIMG = new ImageIcon("assets/images/gamegui/fivehour.png");
	public static final ImageIcon STAGE0DOWNIMG = new ImageIcon("assets/images/gamegui/stage0down.png");
	public static final ImageIcon STAGE0LEFTIMG = new ImageIcon("assets/images/gamegui/stage0left.png");
	public static final ImageIcon STAGE0RIGHTIMG = new ImageIcon("assets/images/gamegui/stage0right.png");
	public static final ImageIcon STAGE0UPIMG = new ImageIcon("assets/images/gamegui/stage0up.png");
	public static final ImageIcon STAGE1DOWNIMG = new ImageIcon("assets/images/gamegui/stage1down.png");
	public static final ImageIcon STAGE1LEFTIMG = new ImageIcon("assets/images/gamegui/stage1left.png");
	public static final ImageIcon STAGE1RIGHTIMG = new ImageIcon("assets/images/gamegui/stage1right.png");
	public static final ImageIcon STAGE1UPIMG = new ImageIcon("assets/images/gamegui/stage1up.png");
	public static final ImageIcon STAGE2DOWNIMG = new ImageIcon("assets/images/gamegui/stage2down.png");
	public static final ImageIcon STAGE2LEFTIMG = new ImageIcon("assets/images/gamegui/stage2left.png");
	public static final ImageIcon STAGE2RIGHTIMG = new ImageIcon("assets/images/gamegui/stage2right.png");
	public static final ImageIcon STAGE2UPIMG = new ImageIcon("assets/images/gamegui/stage2up.png");
	public static final ImageIcon STAGE3DOWNIMG = new ImageIcon("assets/images/gamegui/stage3down.png");
	public static final ImageIcon STAGE3LEFTIMG = new ImageIcon("assets/images/gamegui/stage3left.png");
	public static final ImageIcon STAGE3RIGHTIMG = new ImageIcon("assets/images/gamegui/stage3right.png");
	public static final ImageIcon STAGE3UPIMG = new ImageIcon("assets/images/gamegui/stage3up.png");
	public static final ImageIcon BLANKIMG = new ImageIcon("assets/images/gamegui/blank.png");
	
	public static final ImageIcon GODSTAGE0DOWNIMG = new ImageIcon("assets/images/gamegui/god/stage0down.png");
	public static final ImageIcon GODSTAGE0LEFTIMG = new ImageIcon("assets/images/gamegui/god/stage0left.png");
	public static final ImageIcon GODSTAGE0RIGHTIMG = new ImageIcon("assets/images/gamegui/god/stage0right.png");
	public static final ImageIcon GODSTAGE0UPIMG = new ImageIcon("assets/images/gamegui/god/stage0up.png");
	public static final ImageIcon GODSTAGE1DOWNIMG = new ImageIcon("assets/images/gamegui/god/stage1down.png");
	public static final ImageIcon GODSTAGE1LEFTIMG = new ImageIcon("assets/images/gamegui/god/stage1left.png");
	public static final ImageIcon GODSTAGE1RIGHTIMG = new ImageIcon("assets/images/gamegui/god/stage1right.png");
	public static final ImageIcon GODSTAGE1UPIMG = new ImageIcon("assets/images/gamegui/god/stage1up.png");
	public static final ImageIcon GODSTAGE2DOWNIMG = new ImageIcon("assets/images/gamegui/god/stage2down.png");
	public static final ImageIcon GODSTAGE2LEFTIMG = new ImageIcon("assets/images/gamegui/god/stage2left.png");
	public static final ImageIcon GODSTAGE2RIGHTIMG = new ImageIcon("assets/images/gamegui/god/stage2right.png");
	public static final ImageIcon GODSTAGE2UPIMG = new ImageIcon("assets/images/gamegui/god/stage2up.png");
	public static final ImageIcon GODSTAGE3DOWNIMG = new ImageIcon("assets/images/gamegui/god/stage3down.png");
	public static final ImageIcon GODSTAGE3LEFTIMG = new ImageIcon("assets/images/gamegui/god/stage3left.png");
	public static final ImageIcon GODSTAGE3RIGHTIMG = new ImageIcon("assets/images/gamegui/god/stage3right.png");
	public static final ImageIcon GODSTAGE3UPIMG = new ImageIcon("assets/images/gamegui/god/stage3up.png");
	
	public static final ImageIcon REDSTAGE0DOWNIMG = new ImageIcon("assets/images/gamegui/stun/stage0down.png");
	public static final ImageIcon REDSTAGE0LEFTIMG = new ImageIcon("assets/images/gamegui/stun/stage0left.png");
	public static final ImageIcon REDSTAGE0RIGHTIMG = new ImageIcon("assets/images/gamegui/stun/stage0right.png");
	public static final ImageIcon REDSTAGE0UPIMG = new ImageIcon("assets/images/gamegui/stun/stage0up.png");
	public static final ImageIcon REDSTAGE1DOWNIMG = new ImageIcon("assets/images/gamegui/stun/stage1down.png");
	public static final ImageIcon REDSTAGE1LEFTIMG = new ImageIcon("assets/images/gamegui/stun/stage1left.png");
	public static final ImageIcon REDSTAGE1RIGHTIMG = new ImageIcon("assets/images/gamegui/stun/stage1right.png");
	public static final ImageIcon REDSTAGE1UPIMG = new ImageIcon("assets/images/gamegui/stun/stage1up.png");
	public static final ImageIcon REDSTAGE2DOWNIMG = new ImageIcon("assets/images/gamegui/stun/stage2down.png");
	public static final ImageIcon REDSTAGE2LEFTIMG = new ImageIcon("assets/images/gamegui/stun/stage2left.png");
	public static final ImageIcon REDSTAGE2RIGHTIMG = new ImageIcon("assets/images/gamegui/stun/stage2right.png");
	public static final ImageIcon REDSTAGE2UPIMG = new ImageIcon("assets/images/gamegui/stun/stage2up.png");
	public static final ImageIcon REDSTAGE3DOWNIMG = new ImageIcon("assets/images/gamegui/stun/stage3down.png");
	public static final ImageIcon REDSTAGE3LEFTIMG = new ImageIcon("assets/images/gamegui/stun/stage3left.png");
	public static final ImageIcon REDSTAGE3RIGHTIMG = new ImageIcon("assets/images/gamegui/stun/stage3right.png");
	public static final ImageIcon REDSTAGE3UPIMG = new ImageIcon("assets/images/gamegui/stun/stage3up.png");
	
	public static final ImageIcon IMMUNESTAGE0DOWNIMG = new ImageIcon("assets/images/gamegui/immune/stage0down.png");
	public static final ImageIcon IMMUNESTAGE0LEFTIMG = new ImageIcon("assets/images/gamegui/immune/stage0left.png");
	public static final ImageIcon IMMUNESTAGE0RIGHTIMG = new ImageIcon("assets/images/gamegui/immune/stage0right.png");
	public static final ImageIcon IMMUNESTAGE0UPIMG = new ImageIcon("assets/images/gamegui/immune/stage0up.png");
	public static final ImageIcon IMMUNESTAGE1DOWNIMG = new ImageIcon("assets/images/gamegui/immune/stage1down.png");
	public static final ImageIcon IMMUNESTAGE1LEFTIMG = new ImageIcon("assets/images/gamegui/immune/stage1left.png");
	public static final ImageIcon IMMUNESTAGE1RIGHTIMG = new ImageIcon("assets/images/gamegui/immune/stage1right.png");
	public static final ImageIcon IMMUNESTAGE1UPIMG = new ImageIcon("assets/images/gamegui/immune/stage1up.png");
	public static final ImageIcon IMMUNESTAGE2DOWNIMG = new ImageIcon("assets/images/gamegui/immune/stage2down.png");
	public static final ImageIcon IMMUNESTAGE2LEFTIMG = new ImageIcon("assets/images/gamegui/immune/stage2left.png");
	public static final ImageIcon IMMUNESTAGE2RIGHTIMG = new ImageIcon("assets/images/gamegui/immune/stage2right.png");
	public static final ImageIcon IMMUNESTAGE2UPIMG = new ImageIcon("assets/images/gamegui/immune/stage2up.png");
	public static final ImageIcon IMMUNESTAGE3DOWNIMG = new ImageIcon("assets/images/gamegui/immune/stage3down.png");
	public static final ImageIcon IMMUNESTAGE3LEFTIMG = new ImageIcon("assets/images/gamegui/immune/stage3left.png");
	public static final ImageIcon IMMUNESTAGE3RIGHTIMG = new ImageIcon("assets/images/gamegui/immune/stage3right.png");
	public static final ImageIcon IMMUNESTAGE3UPIMG = new ImageIcon("assets/images/gamegui/immune/stage3up.png");
	//puff clouds
	public static final ImageIcon PUFFIMG = new ImageIcon("assets/images/gamegui/other/puff.png"); 
	//enemies
	public static final ImageIcon DRAGONUPIMG = new ImageIcon("assets/images/gamegui/enemy/dragonup.png");
	public static final ImageIcon DRAGONDOWNIMG = new ImageIcon("assets/images/gamegui/enemy/dragondown.png");
	public static final ImageIcon DRAGONRIGHTIMG = new ImageIcon("assets/images/gamegui/enemy/dragonright.png");
	public static final ImageIcon DRAGONLEFTIMG = new ImageIcon("assets/images/gamegui/enemy/dragonleft.png");
	public static final ImageIcon BEEUPIMG = new ImageIcon("assets/images/gamegui/enemy/beeup.png");
	public static final ImageIcon BEEDOWNIMG = new ImageIcon("assets/images/gamegui/enemy/beedown.png");
	public static final ImageIcon BEERIGHTIMG = new ImageIcon("assets/images/gamegui/enemy/beeright.png");
	public static final ImageIcon BEELEFTIMG = new ImageIcon("assets/images/gamegui/enemy/beeleft.png");
	public static final ImageIcon FLYUPIMG = new ImageIcon("assets/images/gamegui/enemy/flyup.png");
	public static final ImageIcon FLYDOWNIMG = new ImageIcon("assets/images/gamegui/enemy/flydown.png");
	public static final ImageIcon FLYRIGHTIMG = new ImageIcon("assets/images/gamegui/enemy/flyright.png");
	public static final ImageIcon FLYLEFTIMG = new ImageIcon("assets/images/gamegui/enemy/flyleft.png");
	public static final ImageIcon DRONEUPIMG = new ImageIcon("assets/images/gamegui/enemy/droneup.png");
	public static final ImageIcon DRONEDOWNIMG = new ImageIcon("assets/images/gamegui/enemy/dronedown.png");
	public static final ImageIcon DRONERIGHTIMG = new ImageIcon("assets/images/gamegui/enemy/droneright.png");
	public static final ImageIcon DRONELEFTIMG = new ImageIcon("assets/images/gamegui/enemy/droneleft.png");
	public static final ImageIcon HANDUPIMG = new ImageIcon("assets/images/gamegui/enemy/handup.png");
	public static final ImageIcon HANDDOWNIMG = new ImageIcon("assets/images/gamegui/enemy/handdown.png");
	public static final ImageIcon HANDRIGHTIMG = new ImageIcon("assets/images/gamegui/enemy/handright.png");
	public static final ImageIcon HANDLEFTIMG = new ImageIcon("assets/images/gamegui/enemy/handleft.png");
	//gameboard bg 
	public static final ImageIcon WATERIMG = new ImageIcon("assets/images/gamegui/other/water.png");
	
	public static final ImageIcon MILLERIMG = new ImageIcon("assets/images/gamegui/millerpic.png");
	
	//gamepanel win/lose
	public static final ImageIcon WIN = new ImageIcon("assets/images/screens/win.png");
	public static final ImageIcon LOSE = new ImageIcon("assets/images/screens/lose.png");
	
	// ADVERTISEMENTS
	public static final ImageIcon AD1 = new ImageIcon("assets/images/advertisements/cs201ad1.png");
	public static final ImageIcon AD2 = new ImageIcon("assets/images/advertisements/cs201ad2.png");
	public static final ImageIcon AD3 = new ImageIcon("assets/images/advertisements/cs201ad3.png");
	public static final ImageIcon AD4 = new ImageIcon("assets/images/advertisements/cs201ad4.png");
	public static final ImageIcon AD5 = new ImageIcon("assets/images/advertisements/cs201ad5.png");
	public static final ImageIcon AD6 = new ImageIcon("assets/images/advertisements/cs201ad6.png");
	
	//send button 
	public static final ImageIcon SENDBUTTONIMG1 = new ImageIcon("assets/images/buttons/sendbtn_unpressed.png");
	public static final ImageIcon SENDBUTTONIMG2 = new ImageIcon("assets/images/buttons/sendbtn_pressed.png");
	
	//lobby background
	public static final ImageIcon LILYPADBG = new ImageIcon("assets/images/screens/lilypadbg2.png");
	
	//challenge panel 
	public static final ImageIcon ACCEPTBTN1 = new ImageIcon("assets/images/challenge/accept1.png");
	public static final ImageIcon ACCEPTBTN2 = new ImageIcon("assets/images/challenge/accept2.png");
	public static final ImageIcon DECLINEBTN1 = new ImageIcon("assets/images/challenge/decline1.png");
	public static final ImageIcon DECLINEBTN2 = new ImageIcon("assets/images/challenge/decline2.png");
	public static final ImageIcon POLKADOTIMG = new ImageIcon("assets/images/challenge/polka.png");
	
	//endgame button
	public static final ImageIcon ENDGAMEBTN1 = new ImageIcon("assets/images/buttons/endgame.png");
	public static final ImageIcon ENDGAMEBTN2 = new ImageIcon("assets/images/buttons/endgamepressed.png");
	
}
