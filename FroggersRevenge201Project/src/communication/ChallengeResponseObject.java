package communication;

import java.io.Serializable;

public class ChallengeResponseObject implements Serializable {

	private static final long serialVersionUID = 473986284798218871L;
	
	public String challenger;
	public String challenged;
	public boolean accepted;

}
