package communication;

import java.io.Serializable;
import java.util.Vector;

public class PlayerInfo implements Serializable {

	private static final long serialVersionUID = 8594745;
	
		private String username, bestTime, worstTime;
		private int topScore, numEaten, gamesPlayed, gamesWon;
		private Vector<Integer> bestTimeInts, worstTimeInts;
		
		public PlayerInfo(String username, int topScore, int numEaten, String bestTime, String worstTime, int gamesPlayed,
				int gamesWon){
			this.username = username;
			this.topScore = topScore;
			this.numEaten = numEaten;
			this.bestTime = bestTime;
			this.worstTime = worstTime;
			this.gamesPlayed = gamesPlayed;
			this.gamesWon = gamesWon;
			
			bestTimeInts = convertTime(bestTime);
			worstTimeInts = convertTime(worstTime);
		}
		
		//---[ Modifiers ]---
		public void setTopScore(int newTopScore){
			if(newTopScore > topScore){
				topScore = newTopScore;
			}		
		}
		
		public void addNumEaten(int additionalNumEaten){
			numEaten += additionalNumEaten;
		}
		

		public void setBestTime(Vector<Integer> newTime){
			//check starting situation
			if(bestTime.equals("00:00:00")){
				String newBestTime = newTime.get(0)+":"+newTime.get(1)+":"+newTime.get(2);
				bestTime = newBestTime;
				bestTimeInts = newTime;
				return;
			}
			for(int i=0; i<newTime.size(); i++){
				if(newTime.get(i) < bestTimeInts.get(i)){
					String newBestTime = newTime.get(0)+":"+newTime.get(1)+":"+newTime.get(2);
					bestTime = newBestTime;
					bestTimeInts = newTime;
				}
			}
		}
		
		public void setWorstTime(Vector<Integer> newTime){
			for(int i=0; i<newTime.size(); i++){
				if(newTime.get(i) > worstTimeInts.get(i)){
					String newWorstTime = newTime.get(0)+":"+newTime.get(1)+":"+newTime.get(2);
					worstTime = newWorstTime;
					worstTimeInts = newTime;
				}
			}
		}
		
		//time in format: hh:mm:ss
		public void setBestTime(String newBestTime){
			//check starting situation
			if(bestTime.equals("00:00:00")){
				bestTime = newBestTime;
				bestTimeInts = convertTime(newBestTime);
				return;
			}
			Vector<Integer> newTime = convertTime(newBestTime);
			for(int i=0; i<newTime.size(); i++){
				if(newTime.get(i) < bestTimeInts.get(i)){
					bestTime = newBestTime;
					bestTimeInts = newTime;
				}
			}
		}
		
		//time in format: hh:mm:ss
		public void setWorstTime(String newWorstTime){
			Vector<Integer> newTime = convertTime(newWorstTime);
			for(int i=0; i<newTime.size(); i++){
				if(newTime.get(i) < worstTimeInts.get(i)){
					worstTime = newWorstTime;
					worstTimeInts = newTime;
				}
			}
		}
		
		public void incrementGamesPlayed(){
			gamesPlayed++;
		}
		
		public void incrementWins(){
			gamesWon++;
		}
		
		//---[ Accessors ]---
		
		public String getUsername(){
			return username;
		}
		
		public int getTopScore(){
			return topScore;
		}
		
		public int getNumEaten(){
			return numEaten;
		}
		
		public String getBestTime(){
			return bestTime;
		}
		
		public String getWorstTime(){
			return worstTime;
		}
		
		public int getGamesPlayed(){
			return gamesPlayed;
		}
		
		public int getGamesWon(){
			return gamesWon;
		}
		
		//parse hh:mm:ss where each unit can be either one digit or two digits
		private Vector<Integer> convertTime(String time){
			String unit="";
			Vector<Integer> timeInInts = new Vector<Integer>();
			for(int i=0; i<time.length(); i++){
				if(time.substring(i, i+1).equals(":")){
					timeInInts.add(Integer.parseInt(unit));
					unit = "";
				}
				else{
					unit += time.substring(i, i+1);
				}
			}
			//add the last unit of time
			timeInInts.add(Integer.parseInt(unit));
			
			return timeInInts;
		}
}
