package communication;

import java.io.Serializable;

/* Used for logging out/exiting lobby (entering = false)
 * Disconnects (entering = false)
 * Entering lobby in all cases (entering = true) */
public class EnterLobbyObject implements Serializable {
	
	private static final long serialVersionUID = 2845641822839754003L;
	
	public boolean entering;
	public String name;
}
