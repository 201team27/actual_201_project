package communication;

import java.io.Serializable;

public class ChallengeObject implements Serializable {

	private static final long serialVersionUID = -5412696303364769603L;
	
	public String challenger;
	public String challenged;

}
