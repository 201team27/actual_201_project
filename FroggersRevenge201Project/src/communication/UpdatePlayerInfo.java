package communication;

import java.io.Serializable;

public class UpdatePlayerInfo implements Serializable {

	private static final long serialVersionUID = -7769242885526261060L;
	public PlayerInfo pi;
	
	public UpdatePlayerInfo(PlayerInfo pi) {
		this.pi = pi;
	}

}
