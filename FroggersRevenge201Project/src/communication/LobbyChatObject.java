package communication;

import java.awt.Color;
import java.io.Serializable;

public class LobbyChatObject implements Serializable {

	private static final long serialVersionUID = 771189061357896322L;
	
	public String name = "";
	public String msg = "";
	public Color col = Color.BLACK;

}
