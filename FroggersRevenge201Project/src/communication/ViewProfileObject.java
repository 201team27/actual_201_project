package communication;

import java.io.Serializable;

public class ViewProfileObject implements Serializable {
	
	private static final long serialVersionUID = -4508969893491203529L;
	
	public String name;  
	
	public ViewProfileObject(String n) {
		name = n; 
	}
	
}
