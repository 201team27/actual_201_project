package communication;

import java.io.Serializable;

public class EndGameObject implements Serializable{

	private static final long serialVersionUID = 738594;
	
	public PlayerInfo playerInfo=null;
	public boolean win = false;
	
	public EndGameObject(PlayerInfo pi, boolean win){
		this.playerInfo = pi;
		this.win = win;
	}

}
