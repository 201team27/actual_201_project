package communication;

import java.io.Serializable;

public class EvolveObject implements Serializable {

	private static final long serialVersionUID = -7767174268515455983L;
	
	private Integer evolveTo;
	
	public EvolveObject(Integer evolveTo){
		this.evolveTo=evolveTo;
	}
	
	public Integer getLvl() {
		return evolveTo;
	}

}
