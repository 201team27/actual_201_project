package boardthings;

import java.util.Random;

public class OLDNode {
	//private Player player;
	private OLDEnemy enemy=null;
	private OLDItem item=null;
	private boolean lilypad=false;
	private int positionX=0;
	private int positionY=0;
	private OLDNode neighbor_up=null;
	private OLDNode neighbor_down=null;
	private OLDNode neighbor_left=null;
	private OLDNode neighbor_right=null;
	private Random rand;
	//constructor
	public OLDNode(){
		rand=new Random();
	}
	//access node upward
	public OLDNode upward(){
		return neighbor_up;
	}
	//access node downward
	public OLDNode downward(){
		return neighbor_down;
	}
	//access node to the left
	public OLDNode leftward(){
		return neighbor_left;
	}
	//access node to the right
	public OLDNode rightward(){
		return neighbor_right;
	}
	//BOOLEAN returns if node has a lilypad
	public boolean isLilypad(){
		return lilypad;
	}
	//enemy appears
	public void spawnEnemy(){
		enemy=new OLDEnemy(0,this);
	}
	//enemy moves onto this node
	public void assignEnemy(OLDEnemy e){
		if(enemy==null) enemy=e;
	}
	//RETURN enemy
	public OLDEnemy retreiveEnemy(){
		return enemy;
	}
	//remove enemy
	public void removeEnemy(){
		enemy=null;
	}
	//item appears
	public void spawnItem(){
		int choose=rand.nextInt()%3;
		if(choose==0){
			item=new OLDCake(this);
		}
		else if(choose==1){
			item=new OLDCandy(this);
		}
		else if(choose==2){
			item=new OLDFiveHourEnergy(this);
		}
	}
	//RETURN item
	public OLDItem retreiveItem(){
		return item;
	}
	//remove item
	public void removeItem(){
		item=null;
	}
}
