package boardthings;

import game.FrogPlayer;

public class OLDFiveHourEnergy extends OLDItem{
	public OLDFiveHourEnergy(OLDNode n){
		node_on=n;
	}
	@Override
	//increases player speed and adds 50 points
	public void effect(FrogPlayer p) {
		System.out.println("I am five hour energy");
		p.changeSpeed(2);
		p.addPoints(50);
	}

}
