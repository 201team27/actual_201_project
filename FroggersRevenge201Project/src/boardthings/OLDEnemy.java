package boardthings;

import game.FrogPlayer;

import java.awt.Image;
import java.util.Random;

//for enemies that get spawned on the board
public class OLDEnemy {

	private int enemy_level=0;
	private int enemy_points=0;
	private OLDNode node_on;
	private int enemy_dir;//0 is up, 1 is right, 2 is down, 3 is left
	private int enemy_speed=0;
	private Image enemy_image;
	private Random rand;
	
	public OLDEnemy(int playerlevel, OLDNode n){
		node_on=n;
		rand=new Random();
		int enemylevel=rand.nextInt()%(1+playerlevel);
		enemy_dir=rand.nextInt()%4;
		setLVL(enemylevel);
	}
	
	//set level
	public void setLVL(int lvl){
		enemy_level=lvl;
		//enemy level 0 (fly)
		if(lvl==0){
			setSPD(1);
			setPNTS(10);
			//setIMG();
		}
		//enemy level 1 (dragonfly)
		if(lvl==1){
			setSPD(1);
			setPNTS(20);
			//setIMG();
		}
		//enemy level 2 (bee)
		if(lvl==2){
			setSPD(1);
			setPNTS(30);
			//setIMG();
		}
		//enemy level 3 (hand)
		if(lvl==3){
			setSPD(1);
			setPNTS(40);
			//setIMG();
		}
		//enemy level 4 (drone)
		if(lvl==4){
			setSPD(1);
			setPNTS(50);
			//setIMG();
		}
	}
	//set points
	private void setPNTS(int points){
		enemy_points=points;
	}
	//set speed
	public void setSPD(int speed){
		enemy_speed=speed;
	}
	//set image
	private void setIMG(Image img){
		enemy_image=img;
	}
	//get points during eating process
	public int getPNTS(){
		return enemy_points;
	}
	//effect on player
	public void effect(FrogPlayer p){
		if(p.returnLevel()>=enemy_level){
			//eat enemy, remove enemy from node
			p.addPoints(enemy_points);
			node_on.removeEnemy();
			node_on=null;
		}
		else{
			//do something else
		}
	}
	//enemy movement
	public void move(){
		if (enemy_dir==0){
			;
		}
	}
}
