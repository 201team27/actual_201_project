package boardthings;

import game.FrogPlayer;

public class OLDCandy extends OLDItem{
	public OLDCandy(OLDNode n){
		node_on=n;
	}
	@Override
	//adds 200 points
	public void effect(FrogPlayer p) {
		System.out.println("I am candy");
		p.addPoints(500);
	}
	
}
