package boardthings;

import game.FrogPlayer;

import java.awt.Image;

//for items that get spawned on the board
public abstract class OLDItem {
	protected Image item_image;
	protected OLDNode node_on;
	//set image
	public void setNode(OLDNode noden){
		node_on=noden;
	}
	public void setIMG(Image img){
		item_image=img;
	}
	//take effect on player/opponent
	public abstract void effect(FrogPlayer p);
}
