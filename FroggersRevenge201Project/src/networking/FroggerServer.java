package networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import communication.ChallengeObject;
import communication.ChallengeResponseObject;
import communication.EndGameObject;
import communication.EnterLobbyObject;
import communication.EvolveObject;
import communication.GameChatObject;
import communication.LobbyChatObject;
import communication.LoginObject;
import communication.PlayerInfo;
import communication.RegisterObject;
import communication.UpdatePlayerInfo;
import communication.ViewProfileObject;

public class FroggerServer extends Thread {
	
	// holds all connections made to server
	private Vector<FSCThread> allConnectionsVector;
	
	// holds all PlayerInfo objects for connected clients
	private Map<String, PlayerInfo> allClientInfoMap;
	
	// holds all PlayerInfo objects for connected clients
	private Map<FSCThread, String> threadsToNamesMap;
	
	// holds all threads (users) in available in lobby
	private Vector<FSCThread> lobbyVector;
	
	// holds all active game pairings (players exist as key and value - two pairs per game)
	private Map<FSCThread, FSCThread> activeGamesMap;

	// holds all challenges, challenged is key, challenger is value
	// NOTE: probably going to rethink this structure
	private Map<FSCThread, FSCThread> challengesMap;
	
	private ServerSocket ss;
	
	private MySQLDriver sqld;
	
	private Lock dataLock;
	
	public static void main(String[] args) {
		// make the FroggerServer (look through the constructor for details)
		FroggerServer fs = new FroggerServer();
		// start the server running on a separate thread
		// (this was unnecessary but a fine design choice,
		// could have just done the while(true) loop in main)
		fs.start();
	}
	
	public FroggerServer() {
		// initialize data structures, make them threadsafe
		allConnectionsVector = new Vector<FSCThread>();
		lobbyVector = new Vector<FSCThread>();
		allClientInfoMap = new HashMap<String, PlayerInfo>();
		Collections.synchronizedMap(allClientInfoMap);
		threadsToNamesMap = new HashMap<FSCThread, String>();
		Collections.synchronizedMap(threadsToNamesMap);
		activeGamesMap = new HashMap<FSCThread, FSCThread>();
		Collections.synchronizedMap(activeGamesMap);
		challengesMap = new HashMap<FSCThread, FSCThread>();
		Collections.synchronizedMap(challengesMap);
		
		dataLock = new ReentrantLock();
		
		sqld = new MySQLDriver();
		
		
		// git yo serversocket
		try {
			// open up a server socket on port 6666 in order to listen for client connections
			ss = new ServerSocket(6666);
			System.out.println("Bound to port 6666");
		} catch (IOException ioe) {
			System.out.println("ServerSocket in FroggerServer did not initialize successfully.");
			System.out.println("IOException in FroggerServer constructor: " + ioe.getMessage());
		}

	}
	
	public void disconnect(FSCThread fsct) {
		System.out.println("someone disconnected and the server knows!!!!");
		dataLock.lock();
		
		// if dc's during a game
		if (activeGamesMap.containsKey(fsct)) {
			
			// pull them both out of the game
			FSCThread opThread = activeGamesMap.get(fsct);
			activeGamesMap.remove(opThread);
			activeGamesMap.remove(fsct);
			
			// Make opThread win <== send something to client so it calls a method on gamepanel that makes this happen
			
			
			for (int i = 0; i < lobbyVector.size(); i++) {
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.entering = true;
				elo.name = threadsToNamesMap.get(opThread);
				lobbyVector.get(i).toClient(elo);
			}
			
			lobbyVector.add(opThread);
			
			for (int i = 0; i < lobbyVector.size(); i++) {
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.entering = true;
				elo.name = threadsToNamesMap.get(lobbyVector.get(i));
				opThread.toClient(elo);
			}
			
			opThread.toClient("showlobby");
		
		// it someone dc's during a challenge...
		// this is a bad time, we'll work this out later
		} else if (challengesMap.containsKey(fsct)) {
			
			
			
		// if exiting/dc'd from lobby
		} else if (lobbyVector.contains(fsct)) {
			lobbyVector.remove(fsct);
			
			for (int i = 0; i < lobbyVector.size(); i++) {
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.entering = false;
				elo.name = threadsToNamesMap.get(fsct);
				lobbyVector.get(i).toClient(elo);
			}
		} 
		
		// remove from last datastructures
		allConnectionsVector.remove(fsct);
		String deadName = threadsToNamesMap.get(fsct);
		allClientInfoMap.remove(deadName);
		threadsToNamesMap.remove(fsct);
		
		dataLock.unlock();
	}
	
	public void run() {
		try {
			while (true) {
				// create a socket with which to accept client connections
				Socket s;
				// accept a client connection
				// note that while running, the loop hangs here until it receives a connection
				s = ss.accept();
				
				System.out.println("Connected: " + s.getInetAddress());
				
				// create clientserverthread and pass reference to both the socket connection
				// and to the server itself (this)
				FSCThread fsct = new FSCThread(s, this);
				
				// run thread
				fsct.start();
				
				dataLock.lock();
				// add to connections vector
				allConnectionsVector.add(fsct);
				dataLock.unlock();
				//allClientInfoMap.add(fsct, null);
				
			}
		} catch (IOException ioe) {
			System.out.println("IOException in FroggerServer.run(): " + ioe.getMessage());
		} finally {
			try {
				if (ss != null) {
					ss.close();
				}
			} catch (IOException ioe) {
				System.out.println("IOException in FroggerServer.run()(finally): " + ioe.getMessage());
			}
			
			// save data?
		}
	}
	
	public void toServer(Object ob, FSCThread originThread) {
		//System.out.println("Received Object");
		
		dataLock.lock();
		
		if (ob instanceof String) {
			String s = (String)ob;
			//retrieve top scores from database and send to client
			if(s.equals("topscores")){
				sqld.connect();
				Map<String, Integer> scoresMap = sqld.getTopScores();
				originThread.toClient(scoresMap);
			} else if (s.equals("gueststart")) {
				
				String name = "guest" + System.currentTimeMillis();
				
				PlayerInfo pi = new PlayerInfo(name, 0, 0, "00:00:00", "00:00:00", 0, 0);
				
				// send PlayerInfo back to sending client so it can establish itself
				System.out.println("sending pi back to client who requested");
				originThread.toClient(pi);
				allClientInfoMap.put(pi.getUsername(), pi);
				threadsToNamesMap.put(originThread, pi.getUsername());
				
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.name = pi.getUsername();
				elo.entering = true;
				
				for (int i = 0; i < lobbyVector.size(); i++) {					
					String inLobby = threadsToNamesMap.get(lobbyVector.get(i));
					EnterLobbyObject eloIn = new EnterLobbyObject();
					eloIn.entering = true;
					eloIn.name = inLobby;
					originThread.toClient(eloIn);
				}
				
				// add this thread to our lobby in server
				lobbyVector.add(originThread);
				// let all lobby players know you entered lobby (including yourself)
				for (int i = 0; i < lobbyVector.size(); i++) {					
					lobbyVector.get(i).toClient(elo);
				}
				
				
			} else if (s.equals("fivehour")) {
				activeGamesMap.get(originThread).toClient("fivehour");
			} else if (s.equals("cakeisalie")) {
				activeGamesMap.get(originThread).toClient("cakeisalie");
			}
			
		} else if (ob instanceof LobbyChatObject) {
			
			for (int i = 0; i < lobbyVector.size(); i++) {
				if (originThread != lobbyVector.get(i)) {
					lobbyVector.get(i).toClient(ob);
				}
			}
			
			
		} else if (ob instanceof GameChatObject) {
			
			activeGamesMap.get(originThread).toClient(ob);
			
		} else if (ob instanceof RegisterObject) {
			sqld.connect();
			if (!sqld.usernameExists(((RegisterObject)ob).name)) {
				PlayerInfo pi = new PlayerInfo(((RegisterObject)ob).name, 0, 0, "00:00:00", "00:00:00", 0, 0);
				sqld.addPlayer(pi, ((RegisterObject)ob).pass.hashCode());
				originThread.toClient("registeredsuccessfully");
				
			} else {
				originThread.toClient("nametaken");
			}
			sqld.disconnect();
		} else if (ob instanceof LoginObject) {
			sqld.connect();
			//System.out.println("Got LoginObject in FroggerServer");
			// check if valid login
			PlayerInfo pi = sqld.verifyUser(((LoginObject)ob).name,	((LoginObject)ob).pass.hashCode());
			
			// login not valid
			if (pi == null) {
				System.out.println("pi was NULL!");
				originThread.toClient("wronglogin");
			// login valid
				
			} else if (!allClientInfoMap.containsKey(pi.getUsername())) {
				System.out.println(threadsToNamesMap.get(originThread));
				System.out.println("pi username: " + pi.getUsername());
				
				System.out.println(threadsToNamesMap);
				// send PlayerInfo back to sending client so it can establish itself
				//System.out.println("sending pi back to client who requested");
				originThread.toClient(pi);
				allClientInfoMap.put(pi.getUsername(), pi);
				threadsToNamesMap.put(originThread, pi.getUsername());
				
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.name = pi.getUsername();
				elo.entering = true;
				
				for (int i = 0; i < lobbyVector.size(); i++) {					
					String inLobby = threadsToNamesMap.get(lobbyVector.get(i));
					EnterLobbyObject eloIn = new EnterLobbyObject();
					eloIn.entering = true;
					eloIn.name = inLobby;
					originThread.toClient(eloIn);
				}
				
				// add this thread to our lobby in server
				lobbyVector.add(originThread);
				// let all lobby players know you entered lobby (including yourself)
				for (int i = 0; i < lobbyVector.size(); i++) {					
					lobbyVector.get(i).toClient(elo);
				}
			} else {
				originThread.toClient("alreadyin");
			}
			
			sqld.disconnect();
			
		} else if (ob instanceof ViewProfileObject) {
			String opName = ((ViewProfileObject)ob).name;
			PlayerInfo pi = allClientInfoMap.get(opName);
			originThread.toClient(pi);
		} else if (ob instanceof EnterLobbyObject) {
			EnterLobbyObject elo = (EnterLobbyObject)ob;
			String playerName = elo.name;
			//if exiting the lobby (logging out or disconnecting)
			if(!elo.entering) {
				allClientInfoMap.remove(playerName);
				threadsToNamesMap.remove(originThread);
				lobbyVector.remove(originThread);
				//send to all 
				for (int i = 0; i < lobbyVector.size(); i++) {					
					lobbyVector.get(i).toClient(elo);
				}
			}
		// someone is challenging someone else
		} else if (ob instanceof ChallengeObject) {
			String challenger = ((ChallengeObject)ob).challenger;
			String challenged = ((ChallengeObject)ob).challenged;
			FSCThread challengedThread = null;
			
			for(FSCThread curThread : threadsToNamesMap.keySet()) {
				if (threadsToNamesMap.get(curThread).equals(challenged)) {
					challengedThread = curThread;
				}
			}
			
			// this could be a problem if our lists don't work or something
			// ie if challengedThread isn't found and remains null
			EnterLobbyObject elo1 = new EnterLobbyObject();
			elo1.entering = false;
			elo1.name = challenger;
			
			EnterLobbyObject elo2 = new EnterLobbyObject();
			elo2.entering = false;
			elo2.name = challenged;
			
			// tell everyone these two are leaving the lobby (get them out of lists)
			for (FSCThread curThread : lobbyVector) {
				curThread.toClient(elo1);
				curThread.toClient(elo2);
			}
			
			// take them out of the lobby
			lobbyVector.remove(originThread);
			lobbyVector.remove(challengedThread);
			
			// put them in the challenges map
			challengesMap.put(challengedThread, originThread);
			
			// send the challenge to the challenged player
			challengedThread.toClient(ob);
		} else if(ob instanceof ChallengeResponseObject) {
			FSCThread challengerThread = challengesMap.get(originThread);
			challengesMap.remove(originThread);
			// if the challenge was accepted
			if (((ChallengeResponseObject)ob).accepted) {
				
				activeGamesMap.put(originThread, challengerThread);
				activeGamesMap.put(challengerThread, originThread);
				
			// if the challenge was not accepted
			} else {
				//threadsToNamesMap
				lobbyVector.add(originThread);
				lobbyVector.add(challengerThread);
				
				EnterLobbyObject elo1 = new EnterLobbyObject();
				elo1.entering = true;
				elo1.name = threadsToNamesMap.get(originThread);
				
				EnterLobbyObject elo2 = new EnterLobbyObject();
				elo2.entering = true;
				elo2.name = threadsToNamesMap.get(challengerThread);
				
				for (FSCThread curThread : lobbyVector) {
					curThread.toClient(elo2);
					curThread.toClient(elo1);
				}				
			}
			
			originThread.toClient(ob);
			challengerThread.toClient(ob);	
		} else if (ob instanceof EndGameObject) {
		
			// Game just ended
			if (activeGamesMap.containsKey(originThread)){
				FSCThread op = activeGamesMap.get(originThread);
				
				activeGamesMap.remove(originThread);
				activeGamesMap.remove(op);
				
				String origName = threadsToNamesMap.get(originThread);
				if (!origName.startsWith("guest")) {
					PlayerInfo pi = ((EndGameObject)ob).playerInfo;
					
					sqld.connect();
					
					sqld.updatePlayer(pi);
					
					sqld.disconnect();
					
					UpdatePlayerInfo upi = new UpdatePlayerInfo(pi);
					originThread.toClient(upi);
					
					allClientInfoMap.put(origName, pi);
				}
				
				for (int i = 0; i < lobbyVector.size(); i++) {					
					String inLobby = threadsToNamesMap.get(lobbyVector.get(i));
					EnterLobbyObject eloIn = new EnterLobbyObject();
					eloIn.entering = true;
					eloIn.name = inLobby;
					originThread.toClient(eloIn);
				}
				
				lobbyVector.add(originThread);
				
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.entering = true;
				elo.name = origName;
				for (int i = 0; i < lobbyVector.size(); i++) {		
					
					lobbyVector.get(i).toClient(elo);
				}
				
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				originThread.toClient("showlobby");
				
				if (((EndGameObject)ob).win) {
					op.toClient("lose");
				} else {
					op.toClient("win");
				}				
				
			} else {
				
				String origName = threadsToNamesMap.get(originThread);
				
				if (!origName.startsWith("guest")) {
					PlayerInfo pi = ((EndGameObject)ob).playerInfo;
					
					sqld.connect();
					
					sqld.updatePlayer(pi);
					
					sqld.disconnect();
					
					UpdatePlayerInfo upi = new UpdatePlayerInfo(pi);
					originThread.toClient(upi);
					
					allClientInfoMap.put(origName, pi);
				}
				
				for (int i = 0; i < lobbyVector.size(); i++) {					
					String inLobby = threadsToNamesMap.get(lobbyVector.get(i));
					EnterLobbyObject eloIn = new EnterLobbyObject();
					eloIn.entering = true;
					eloIn.name = inLobby;
					originThread.toClient(eloIn);
				}
				
				lobbyVector.add(originThread);
				
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.entering = true;
				elo.name = origName;
				for (int i = 0; i < lobbyVector.size(); i++) {		
					
					lobbyVector.get(i).toClient(elo);
				}
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				originThread.toClient("showlobby");
			}	
				
		} else if (ob instanceof EvolveObject) {
			if (activeGamesMap.containsKey(originThread)) {
				activeGamesMap.get(originThread).toClient(ob);
			}
		}
	 	
		dataLock.unlock();
	}
}
