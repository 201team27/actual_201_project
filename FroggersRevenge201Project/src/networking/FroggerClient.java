package networking;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

import communication.ChallengeObject;
import communication.ChallengeResponseObject;
import communication.EnterLobbyObject;
import communication.EvolveObject;
import communication.GameChatObject;
import communication.LobbyChatObject;
import communication.PlayerInfo;
import communication.UpdatePlayerInfo;
import gameclient.AdvertisementThread;
import gameclient.ClientPanel;
import gameclient.GamePanel;
import library.Constants;

public class FroggerClient extends Thread {
	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	//private GameManager gm;
	private ClientPanel cp;
	
	private PlayerInfo playerInfo;
	private AdvertisementThread ads;
	
	public FroggerClient(ClientPanel cp) {
		this.cp = cp;
		playerInfo = null;
		ads = null;
		
		try {
			
			Scanner sc = new Scanner(new File("resources/server_address.txt"));
			
			String ipAddress = sc.next();
			
			//NOTE: we will have to modify this to read in from a file for the appropriate ip address
			s = new Socket(ipAddress,6666);
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.flush();
			ois = new ObjectInputStream(s.getInputStream());
						
			sc.close();
		} catch (IOException ioe) {
			System.out.println("IOException in FroggerClient constructor: " + ioe.getMessage());
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void send(Object ob) {
		try {
//			if (ob.equals("hackintolobby")) {
//				cp.showLobby();
//			}
			oos.writeObject(ob);
			oos.flush();
		} catch (IOException ioe) {
			System.out.println("IOException in FroggerClient.send(): " + ioe.getMessage());
		}
	}
	
	public void showGame() {
		cp.showGame();
	}
	
	public void showLobby() {
		cp.showLobby();
	}
	
	public void showLogin() {
		cp.showLogin();
	}
	
	public void showStart() {
		cp.showStart();
	}
	
	public PlayerInfo getPlayerInfo() {
		return playerInfo;
	}
	
	//---[ Logout ]---//
	
	public void logout(){
		stopAds();
		playerInfo = null;
		cp.showStart();
	}
	
	public void stopAds() {
		if (ads != null) {
			ads.interrupt();
		}
		ads = null;
	}
	
	public GamePanel getGamePanel(){
		return cp.getGamePanel();
	}

	public void run() {
		try {
			while (true) {
				Object ob = ois.readObject();
				
				if (ob == null) {
					System.out.println("ob == null");
					break;
				} else {
					//System.out.println("Object Received");
					if (ob instanceof String) {
						if (ob.equals("nametaken")){
							cp.getLoginPanel().registerUnsuccessful();
							
						} else if (ob.equals("registeredsuccessfully")) {
							cp.getLoginPanel().registerSuccessful();
							
						} else if (ob.equals("cantlogin")) {
							cp.getLoginPanel().loginUnsuccessful();
							
						} else if (ob.equals("showlobby")) {
							cp.showLobby();
							
						} else if (ob.equals("showlogin")) {
							cp.showLogin();
						} else if (ob.equals("alreadyin")) {
							cp.getLoginPanel().userAlreadyIn();
						} else if (ob.equals("wronglogin")){
							cp.getLoginPanel().wrongLogin();
						} else if (ob.equals("fivehour")) {
							cp.getGamePanel().getGameBoard().fiveHour();
							cp.getGamePanel().printFive(false);
						} else if (ob.equals("cakeisalie")) {
							getGamePanel().printCake(false);
						} else if (ob.equals("win")) {
							cp.getGamePanel().getGameManager().winGame();
						} else if (ob.equals("lose")) {
							cp.getGamePanel().getGameManager().loseGame();
						}
						
					} else if (ob instanceof PlayerInfo) {
						//System.out.println("Received PlayerInfo object in FroggerClient");
						if (playerInfo == null) {
							playerInfo = (PlayerInfo)ob;
							cp.getLobbyPanel().setPlayerProfile(playerInfo);
							//System.out.println("About to call showLobby() from FroggerClient");
							cp.showLobby();
							cp.getLoginPanel().clearFields();
							if (playerInfo.getUsername().startsWith("guest")) {
								ads = new AdvertisementThread();
							}
							// show lobby
						} else { 
							// Displaying profile screen 
							cp.getLobbyPanel().showProfileScreen((PlayerInfo)ob); 
							//System.out.println("Viewing Player Profile"); 

						}
					
					} else if (ob instanceof LobbyChatObject) {
						String name = ((LobbyChatObject)ob).name;
						String msg = ((LobbyChatObject)ob).msg;
						Color col = ((LobbyChatObject)ob).col;
						
						cp.getLobbyPanel().getLobbyChatPanel().printOut(name, msg, col);
						
					} else if (ob instanceof GameChatObject) {
						
						cp.getGamePanel().printOut(((GameChatObject)ob).name, ((GameChatObject)ob).msg, false);
						
					} else if (ob instanceof EnterLobbyObject) {
						if (((EnterLobbyObject)ob).entering) {
							cp.getLobbyPanel().addPlayer(((EnterLobbyObject)ob).name);
							if ((((EnterLobbyObject)ob).name).equals(playerInfo.getUsername())) {
								cp.showLobby();
							}
						}
						else{
							//leaving lobby (logging out a particular player = remove from player list)
							cp.getLobbyPanel().removePlayer(((EnterLobbyObject)ob).name);
						}
					} else if (ob instanceof ChallengeObject) {
						cp.getLobbyPanel().challenged(((ChallengeObject)ob).challenger);
					} else if (ob instanceof ChallengeResponseObject) {
						// if the challenge was accepted
						if (((ChallengeResponseObject)ob).accepted) {
							
							// RIGHT HERE!!!: MAKE GAME GET SET UP PROPERLY BY
							// CALLING METHODS ON THE CLIENTPANEL, GAMEPANEL, ETC
							
							cp.getGamePanel().getGameManager().startGame();
							
							cp.showGame();
							cp.getLobbyPanel().leaveLobbyScreen();
							
						// if challenge was not accepted:
						} else {
							cp.getLobbyPanel().enableLobbyFunction();
						}
						
					} else if (ob instanceof Map){
						cp.showTopScores((Map<String, Integer>)ob);
						System.out.println("showing top scores?");
					} else if (ob instanceof UpdatePlayerInfo) {
						//playerInfo = ((UpdatePlayerInfo)ob).pi;
						cp.getLobbyPanel().setPlayerProfile(playerInfo);
						cp.getLobbyPanel().getLobbyChatPanel().clearText();
					} else if (ob instanceof EvolveObject){
						int num = ((EvolveObject)ob).getLvl();
						if (num == 0) {
							cp.getGamePanel().changeTheirPic(Constants.STAGE0UPIMG);
						} else if (num == 1) {
							cp.getGamePanel().changeTheirPic(Constants.STAGE1UPIMG);
						} else if (num == 2) {
							cp.getGamePanel().changeTheirPic(Constants.STAGE2UPIMG);
						} else {
							cp.getGamePanel().changeTheirPic(Constants.STAGE3UPIMG);
						}
					}
					
				}
			}
		} catch (IOException ioe) {
			System.out.println("IOException in FroggerClient.run(): " + ioe.getMessage());
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException in FroggerClient.run(): " + cnfe.getMessage());
			
		} finally {
			// clean up client and variables
			// send back to title screen
			try {
				if (oos != null) {
					oos.close();
				}
				if (ois != null) {
					ois.close();
				}
				if (s != null) {
					s.close();
				}
				
			} catch (IOException ioe) {
				System.out.println("IOException in FroggerClient.run()(finally -> catch): " + ioe.getMessage());
			}
			//System.out.println("finished this thread");
		}
	}
}
