package networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import communication.PlayerInfo;

public class FSCThread extends Thread {

	private Socket s;
	private FroggerServer serv;
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	
	public FSCThread(Socket s, FroggerServer serv) {
		// socket that holds the client
		this.s = s;
		// the server the client connected to
		this.serv = serv;

		try {
			// talks back to client
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.flush();
			// receives messages from the client
			ois = new ObjectInputStream(s.getInputStream());
			
		} catch (IOException ioe) {
			System.out.println("ioe in ChatThread constructor: " + ioe.getMessage());
		}
	}
	
	public void toClient(Object ob) {
		if (ob != null) {
			try {
//				if (ob instanceof PlayerInfo) {
//					System.out.println("sending PlayerInfo from FSCThread to client");
//				}
				oos.writeObject(ob);
				oos.flush();
			} catch (IOException ioe) {
				System.out.println("IOException in ServerClientThread.sendObject(): " + ioe.getMessage());
			}
		}
	}
	
	public void run() {
		try {
			while (true) {
				// gets message from client
				Object ob = ois.readObject();
				
				// if client disconnects, kill thread
				if (ob == null) {
					//System.out.println("NULL in while/if");
					break;
				}
//				System.out.println("Sending object to server");
				// sends message to server
				serv.toServer(ob, this);
			}
			
		} catch (IOException ioe) {
			//System.out.println("NULL in catch");
			
			System.out.println("IOException in FSCThread.run(): " + ioe.getMessage());
		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException in FSCThread.run(): " + cnfe.getMessage());
			
		} finally {
			serv.disconnect(this);
			try {
				if (ois != null) {
					ois.close();
				}
				if (oos != null) {
					oos.close();
				}
				if (s != null) {
					s.close();
				}
				
			} catch (IOException ioe) {
				System.out.println("IOException closing FSCThread.run()(finally -> catch): " + ioe.getMessage());
			}
		}
	}
}
