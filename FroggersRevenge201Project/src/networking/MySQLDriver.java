package networking;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import com.mysql.jdbc.Driver;

import communication.PlayerInfo;

public class MySQLDriver {
	
	private Connection con;
	private final static String addPlayer = 
			"INSERT INTO Player(username, topscore, "
			+ "numEaten, bestTime, worstTime, gamesPlayed, gamesWon, "
			+ "pass) VALUES(?,?,?,?,?,?,?,?)";
	private final static String getUserByPass = "SELECT * FROM Player WHERE pass=? AND username=?";
	private final static String getUserByName = "SELECT * FROM Player WHERE username=?";
	private final static String getUsername = "SELECT playerID FROM Player WHERE username=?";
	private final static String getUserTopScore = "SELECT username, topScore FROM Player";
	private final static String updateInfo = "UPDATE Player SET topScore=?, numEaten=?, "
			+ "bestTime=?, worstTime=?, gamesPlayed=?, gamesWon=? WHERE username=?";
	
	public MySQLDriver(){
		try{
			new Driver();
		}catch(SQLException e){
			System.out.println("sql exception in MySQLDriver: " + e.getMessage());
		}
	}
	
	public void connect(){
		try{
			// CHANGE PASSWORD TO WORK WITH YOUR MySQL
			con = DriverManager.getConnection("jdbc:mysql://"
					+ "localhost:3306/AnythingGoes?user=root&password=");
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void disconnect(){
		try{
			con.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	// returns PlayerInfo if password is valid for this username
	public PlayerInfo verifyUser(String username, int encryptedPass){
		try{
			PreparedStatement ps = con.prepareStatement(getUserByPass);
			ps.setInt(1, encryptedPass);
			ps.setString(2, username);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				//String name = result.getString("username");
				
					int topScore = result.getInt("topScore");
					int numEaten = result.getInt("numEaten");
					String bestTime = result.getString("bestTime");
					String worstTime = result.getString("worstTime");
					int gamesPlayed = result.getInt("gamesPlayed");
					int gamesWon = result.getInt("gamesWon");
					PlayerInfo playerInfo = new PlayerInfo(username, topScore, 
							numEaten, bestTime, worstTime, gamesPlayed, gamesWon);
					return playerInfo;
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	//returns true if user already exists
	public boolean usernameExists(String username){
		try{
			PreparedStatement ps = con.prepareStatement(getUsername);
			ps.setString(1, username);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				return true;
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	//add a new player to the database
	public void addPlayer(PlayerInfo playerInfo, int encryptedPass){
		try{
			PreparedStatement ps = con.prepareStatement(addPlayer);
			ps.setString(1, playerInfo.getUsername());
			ps.setInt(2, playerInfo.getTopScore());
			ps.setInt(3, playerInfo.getNumEaten());
			ps.setString(4, playerInfo.getBestTime());
			ps.setString(5, playerInfo.getWorstTime());
			ps.setInt(6,  playerInfo.getGamesPlayed());
			ps.setInt(7, playerInfo.getGamesWon());
			ps.setInt(8, encryptedPass);
			ps.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	//updates user information
	public void updatePlayer(PlayerInfo playerInfo){
		PreparedStatement ps2;
		
		System.out.println("Player: "+playerInfo.getUsername());
		System.out.println("games played: "+playerInfo.getGamesPlayed());
		System.out.println("games won: "+playerInfo.getGamesWon());
		try {
			ps2 = con.prepareStatement(updateInfo);
			ps2.setInt(1, playerInfo.getTopScore());
			ps2.setInt(2, playerInfo.getNumEaten());
			ps2.setString(3, playerInfo.getBestTime());
			ps2.setString(4, playerInfo.getWorstTime());
			ps2.setInt(5,  playerInfo.getGamesPlayed());
			ps2.setInt(6, playerInfo.getGamesWon());
			ps2.setString(7, playerInfo.getUsername());
			ps2.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	//return a map of username, topScores
	public Map<String, Integer> getTopScores(){
		try{
			
			Map<String, Integer> scoresMap = new HashMap<String, Integer>();
			
			//query database 
			PreparedStatement ps = con.prepareStatement(getUserTopScore);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				scoresMap.put(result.getString("username"), new Integer(result.getInt("topScore")));
			}
			return scoresMap;
			
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}

}
