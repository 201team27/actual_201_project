package game;

import javax.swing.ImageIcon;

import library.Constants;

public class Enemy extends Thread{
	private int x, y; 
	private int xCord, yCord; 
	private GuiEnemy gE; 
	private boolean eaten; 
	private int level;
	private String direction;
	private GameBoard gameBoard;
	private ImageIcon movingImg;
	private int speed;
	
	
	
	public Enemy(Node node, int level, GameBoard gameBoard){
		x = node.getX();
		y = node.getY();
		this.level = level;
		this.gameBoard = gameBoard;
		eaten = false;
		
		
		xCord = gameBoard.getGameGui().getWidth() * x / 8;
		yCord = gameBoard.getGameGui().getHeight() * y / 8;
		
		// figure out which picture we need to use		
		// enemy is fly
		if (level == 0) {
			speed = 3;
			// fly going from east(left) to west(right)
			if (x == 0) {
				movingImg = Constants.FLYRIGHTIMG;
				direction = "right";
			// fly going from west(right) to east(left)
			} else if (x == 7) {
				movingImg = Constants.FLYLEFTIMG;
				direction = "left";
			// fly going from north to south
			} else if (y == 0) {
				movingImg = Constants.FLYDOWNIMG;
				direction = "down";
			// fly going from south to north
			} else {
				movingImg = Constants.FLYUPIMG;
				direction = "up";
			}	
		// enemy is bee
		} else if (level == 1) {
			// bee going from east(left) to west(right)
			speed = 4;
			if (x == 0) {
				movingImg = Constants.BEERIGHTIMG;
				direction = "right";
			// bee going from west(right) to east(left)
			} else if (x == 7) {
				movingImg = Constants.BEELEFTIMG;
				direction = "left";
			// bee going from north to south
			} else if (y == 0) {
				movingImg = Constants.BEEDOWNIMG;
				direction = "down";
			// bee going from south to north
			} else {
				movingImg = Constants.BEEUPIMG;
				direction = "up";
			}		
		
		// enemy is dragon fly
		} else if (level == 2) {
			speed = 5;
			// dragon fly going from east(left) to west(right)
			if (x == 0) {
				movingImg = Constants.DRAGONRIGHTIMG;
				direction = "right";				
			// dragon fly going from west(right) to east(left)
			} else if (x == 7) {
				movingImg = Constants.DRAGONLEFTIMG;
				direction = "left";				
			// dragon fly going from north to south
			} else if (y == 0) {
				movingImg = Constants.DRAGONDOWNIMG;
				direction = "down";
			// dragon fly going from south to north
			} else {
				movingImg = Constants.DRAGONUPIMG;
				direction = "up";
			}
					
		// enemy is hand
		} else if (level == 3) {
			speed = 2;
			// hand going from east(left) to west(right)
			if (x == 0) {
				movingImg = Constants.HANDRIGHTIMG;
				direction = "right";
			// hand going from west(right) to east(left)
			} else if (x == 7) {
				movingImg = Constants.HANDLEFTIMG;
				direction = "left";
			// hand going from north to south
			} else if (y == 0) {
				movingImg = Constants.HANDDOWNIMG;
				direction = "down";
			// hand going from south to north
			} else {
				movingImg = Constants.HANDUPIMG;
				direction = "up";
			}
		// enemy is drone
		} else {
			speed = 6;
			// drone going from east(left) to west(right)
			if (x == 0) {
				movingImg = Constants.DRONERIGHTIMG;
				direction = "right";
			// drone going from west(right) to east(left)
			} else if (x == 7) {
				movingImg = Constants.DRONELEFTIMG;
				direction = "left";
			// hand going from north to south
			} else if (y == 0) {
				movingImg = Constants.DRONEDOWNIMG;
				direction = "down";
			// hand going from south to north
			} else {
				movingImg = Constants.DRONEUPIMG;
				direction = "up";				
			}
		}
		
		start();
	}
	
	public void run(){
		// add Cloud graphic here
		gE = new GuiEnemy(Constants.PUFFIMG, xCord, yCord);
		gameBoard.getGameGui().enemyLock.lock();
		gameBoard.getGameGui().getEnemies().add(gE);
		gameBoard.getGameGui().enemyLock.unlock();
		try {
			
			
			long startTime = System.currentTimeMillis();
			long currentTime = startTime;
			
			while (800 > (currentTime - startTime)) {
				currentTime = System.currentTimeMillis();
				Thread.sleep(100);
			}
			
			gE.setImage(movingImg);
			
			startTime = System.currentTimeMillis();
		
			while (14000 > speed*(currentTime - startTime) && !eaten) {
				if (direction.equals("right")) {
					xCord = gameBoard.getGameGui().getWidth() * speed / 8 * (int)(currentTime - startTime) / 2000;
					x = (int)(((double)(currentTime - startTime)*speed / 2000) + .5);
					gE.setX(xCord);
					
				} else if (direction.equals("left")) {
					xCord = gameBoard.getGameGui().getWidth() * 7 / 8 - gameBoard.getGameGui().getWidth() * speed / 8 * (int)(currentTime - startTime) / 2000;
					x = 8 - (int)Math.ceil(((double)(currentTime - startTime)*speed / 2000) + .5);
					gE.setX(xCord);	
				} else if (direction.equals("down")) {
					yCord = gameBoard.getGameGui().getHeight() * speed / 8 * (int)(currentTime - startTime) / 2000;
					y = (int)(((double)(currentTime - startTime)*speed / 2000) + .5);
					gE.setY(yCord);
					
				} else {
					yCord = gameBoard.getGameGui().getHeight() * 7 / 8 - gameBoard.getGameGui().getHeight() * speed / 8 * (int)(currentTime - startTime) / 2000;
					y = 8 - (int)Math.ceil(((double)(currentTime - startTime)*speed / 2000) + .5);
					gE.setY(yCord);	
				} 
				
				
				
				// frog on this node?
				if (gameBoard.hasFrog(x, y)) {
					// get eaten
					if (gameBoard.getGod() || gameBoard.getEvoLevel() >= level) {
						eaten = true;
						gameBoard.addEvoPoints(level + 1);
						gameBoard.addPoints((level + 1) * 4);
						
					} else if (!gameBoard.getImmunity() && !gameBoard.getStunned()) {
						gameBoard.decreaseHealth();
						StunThread st = new StunThread(gameBoard, 2);						
					}
				}
				
				
				
				currentTime = System.currentTimeMillis();
				Thread.sleep(50);
			}
		
			
			
			gE.setImage(Constants.PUFFIMG);
			startTime = System.currentTimeMillis();
			
			while (800 > (currentTime - startTime)) {
				currentTime = System.currentTimeMillis();
				Thread.sleep(100);
			}
		
		} catch (InterruptedException ie) {
			
		} finally {
			gameBoard.getGameGui().enemyLock.lock();
			gameBoard.getGameGui().getEnemies().remove(gE);
			gameBoard.getGameGui().enemyLock.unlock();
		}
	}
}
