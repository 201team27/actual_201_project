package game;

public class StunThread extends Thread{
	private GameBoard gb;
	private int secsOfEffect;
	
	public StunThread(GameBoard gb,int secsOfEffect){
		this.gb=gb;
		this.secsOfEffect=secsOfEffect;
		start();
	}
	
	public void run(){
		try{
			gb.setStunned(true);
			
			Long startTime = System.currentTimeMillis();
			gb.getGameGui().setRed();
			while (1000*secsOfEffect > (System.currentTimeMillis() - startTime)) {
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			gb.setStunned(false);
			gb.getGameGui().setNormal();
		}
	}
}
