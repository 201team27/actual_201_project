package game;

public class GodThread extends Thread{

	private GameBoard gb;
	private int secsOfEffect;
	
	public GodThread(GameBoard gb,int secsOfEffect){
		this.gb=gb;
		this.secsOfEffect=secsOfEffect;
	}
	
	public void run(){
		try{
			gb.getGameGui().setGod(); 
			gb.setGod(true);
			Long startTime = System.currentTimeMillis();
			while (1000*secsOfEffect > (System.currentTimeMillis() - startTime)) {
				if(gb.getGameGui().getType() < 3){
					gb.getGameGui().setGod();
				}
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			gb.setGod(false);
			gb.getGameGui().setNormal(); 
		}
	}
}
