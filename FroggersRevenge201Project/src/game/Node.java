//NODE
package game;

import java.util.Random;

public class Node {
	//private Player player;
//	private Enemy enemy=null;
	private GameItem item=null;
	private boolean lilyPad=false;
	private int positionX=0;
	private int positionY=0;
	private Node neighbor_up=null;
	private Node neighbor_down=null;
	private Node neighbor_left=null;
	private Node neighbor_right=null;
	private Random rand;
	//constructor
	
	// Dummy constructor for testing purposes 
	public Node() {
		rand = new Random();
	}
	
	public Node(boolean lp,int X,int Y, Node up, Node down, Node left, Node right){
		rand=new Random();
		lilyPad=lp;
		positionX=X;
		positionY=Y;
		neighbor_up=up;
		neighbor_down=down;
		neighbor_left=left;
		neighbor_right=right;
	}
	//sets X coordinate
	public void setXPos(int x){
		positionX=x;
	}
	//sets Y coordinate
	public void setYPos(int y){
		positionY=y;
	}
	
	//get positions
	public int getX(){
		return positionX; 
	}
	
	public int getY(){
		return positionY;
	}
	
	//sets lilypad status
	public void setIsPad(Boolean b){
		lilyPad=b;
	}
	
	public boolean isPad(){
		return lilyPad; 
	}
	
	//access node upward
	public Node upward(){
		return neighbor_up;
	}
	//access node downward
	public Node downward(){
		return neighbor_down;
	}
	//access node to the left
	public Node leftward(){
		return neighbor_left;
	}
	//access node to the right
	public Node rightward(){
		return neighbor_right;
	}
	//set node upward
	public void setUpPad(Node n){
		neighbor_up=n;
	}
	//set node downward
	public void setDownPad(Node n){
		neighbor_down=n;
	}
	//set node to the left
	public void setLeftPad(Node n){
		neighbor_left=n;
	}
	//set node to the right
	public void setRightPad(Node n){
		neighbor_right=n;
	}
	//enemy appears
//	public void spawnEnemy(){
//		enemy=new Enemy(0,this);
//	}
//	//enemy moves onto this node
//	public void assignEnemy(Enemy e){
//		if(enemy==null) enemy=e;
//	}
//	//RETURN enemy
//	public Enemy retreiveEnemy(){
//		return enemy;
//	}
//	//remove enemy
//	public void removeEnemy(){
//		enemy=null;
//	}
	//return item which appears
	//RETURN item
	
	public GameItem getGameItem(){
		return item;
	}
	//remove item
	public void removeItem(){
		item=null;
	}
	
	public void setItem(String itemy){
		if(itemy.equals("cake")){
			item = new Cake(); 
		}else if(itemy.equals("fivehour")){
			item = new FiveHourEnergy(); 
		}else if(itemy.equals("candy")){
			item = new Candy(); 
		}else if(itemy.equals("cooie")){
			item = new Cookie(); 
		}else{
			System.out.println("Item does not exist FUDGE");
		}
	}
}
