package game;

import gameclient.GameGui;
import gameclient.GamePanel;

public class RepaintThread extends Thread {
	
	private GameGui gg;
	private GamePanel gp;
	
	public RepaintThread(GameGui gg, GamePanel gp) {
		this.gg = gg;
		this.gp = gp;
	}
	
	public void run() {
		try {
			long startTime = System.currentTimeMillis();
			while (true) {
				gp.updateTime(System.currentTimeMillis() - startTime);
				gg.repaint();
				gg.revalidate();
				Thread.sleep(50);
				
			}
		} catch (InterruptedException ie) {
			
		} finally {
			//gp.updateTime(0);
		}
	}

}
