package game;

import java.util.Random;

import game.Node;

public class FrogPlayer {
	private Node current_node=null;
	private int player_level=0;
	private int player_points=0;
	private int player_speed=1;
	private Random rand;
	//constructor
	public FrogPlayer(){
		System.out.println("I made my frog");
		rand=new Random();
		//put frog on node
	}
	//set current node
	public void setNode(Node n){
		current_node=n;
	}
	//level up
	public void levelUp(){
		if (player_level<4) player_level=player_level+1;
		else if (player_level==4){
			;
		}
	}
	
	public void levelDown(){
		if(player_level>0) player_level=player_level+1;
		else if (player_level == 0){
			System.out.println("can't go down any more levels");
		}
	}
	//RETURN player level
	public int returnLevel(){
		return player_level;
	}
	//run into enemy/item
	public void eat(){
		if(current_node!=null){
//			if(current_node.retreiveEnemy()!=null){
//				current_node.retreiveEnemy().effect(this);
//			}
			if(current_node.getGameItem()!=null){
				//current_node.getGameItem().effect(this);
			}
		}
	}
	//add points to player total
	public void addPoints(int new_add){
		player_points = player_points+new_add;
	}
	//change player speed
	public void changeSpeed(int factor){
		player_speed = player_speed*factor;
	}
}
