 package game;

import java.util.Random;
import java.util.Vector;

import communication.EvolveObject;
import gameclient.GameGui;
import library.Constants;
import networking.FroggerClient;

public class GameBoard {
	//    LilyPad[row][col]
	private Node[][] board;
	//private FrogPlayer froggy; // main player
	//NEEDS TO PASS IN GUI
	//MANIPULATES GUI
	//LOOK AT THIS, IT IS IMPORTANT!!!
	private FroggerClient froggerClient;
	private GameGui gameGui; 
	private int playerLocX; 
	private int playerLocY;
	private int score=0;
	private int evoLevel=0;
	private int evoPoints=0;
	private int health=4;
	private boolean isImmune; 
	private boolean isGod; 
	private boolean isStunned;
	private int itemsEaten; 
	private int enemiesEaten;
	private Random rand;
	
	private Vector<Node> padNodes;
	private Vector<Node> edgeNodes;
	
	public GameBoard(GameGui gameGui, FroggerClient fc){
		itemsEaten = 0; 
		enemiesEaten = 0;
		score = 0;
		rand=new Random();
		this.gameGui = gameGui; 
		this.froggerClient=fc;
		board = new Node[8][8];
		//froggy = new FrogPlayer();
		playerLocX = 4; 
		playerLocY = 4;
		
		padNodes = new Vector<Node>();
		edgeNodes = new Vector<Node>();
		
		for (int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				
				// Want to create nodes here 
				board[j][i] = new Node();
				
				// if not on top row
				if (i != 0) {
					board[j][i].setUpPad(board[j][i-1]);
				} else if (!edgeNodes.contains(board[j][i])) {
					edgeNodes.add(board[j][i]);
					
				}
				
				// if not on bottom row
				if (i != 7) {
					board[j][i].setDownPad(board[j][i+1]);
				} else if (!edgeNodes.contains(board[j][i])) {
					edgeNodes.add(board[j][i]);
				}
				
				// if not on left column
				if (j != 0) {
					board[j][i].setLeftPad(board[j-1][i]);
				} else if (!edgeNodes.contains(board[j][i])) {
					edgeNodes.add(board[j][i]);					
				}
				
				// if not on right column
				if (j != 7) {
					board[j][i].setRightPad(board[j+1][i]);
				} else if (!edgeNodes.contains(board[j][i])) {
					edgeNodes.add(board[j][i]);					
				}
				

				if(j == 1 && i == 1 ||
						j ==  2 && i == 1 ||
						j == 6 && i == 0 || 
						j == 4 && i == 2 ||
						j == 7 && i == 2 || 
						j == 0 && i == 3 || 
						j == 2 && i == 4 || 
						j == 3 && i == 4 || 
						j == 5 && i == 4 || 
						j == 1 && i == 5 ||
						j == 5 && i == 5 || 
						j == 7 && i == 5 || 
						j == 3 && i == 6 || 
						j == 5 && i == 6){
					board[j][i].setIsPad(false);
				}else{
					board[j][i].setIsPad(true);
					padNodes.add(board[j][i]);
				}
				
				//board[j][i].setGameItem(null);
				//board[j][i].setGameFrog(null);
				board[j][i].setXPos(j); 
				board[j][i].setYPos(i);
			}
		}
	}
	
	public void addPoints(int points) {
		score += points;
		froggerClient.getGamePanel().updateScore(score);
		
	}
	
	public Vector<Node> getPadNodes() {
		return padNodes;
	}
	
	public Vector<Node> getEdgeNodes() {
		return edgeNodes;
	}
	
	public GameGui getGameGui() {
		return gameGui;
	}
	
	public void setImmunity(boolean x){
		isImmune = x;
	}
	
	public void setGod(boolean x){
		isGod = x; 
	}
	
	public void setStunned(boolean x){
		isStunned = x;
	}
	
	public boolean getImmunity(){
		return isImmune; 
	}
	
	public boolean getGod(){
		return isGod; 
	}
	
	public boolean getStunned(){
		return isStunned; 
	}
	
	public boolean hasFrog(int x, int y) {
		return (x == playerLocX && y == playerLocY);
	}
	
	public int getEvoLevel() {
		return evoLevel;
	}
	
	public void updatePlayer(String item){
		itemsEaten = itemsEaten + 1; 
		if(item.equals("candy")){
			score += 20;
			froggerClient.getGamePanel().updateScore(score);
			ImmunityThread imT = new ImmunityThread(this, 8);
			imT.start();			
		}else if(item.equals("cookie")){
			//make god
			score += 10;
			froggerClient.getGamePanel().updateScore(score);
			GodThread goT = new GodThread(this, 8);
			goT.start();
		}else if(item.equals("fivehour")){
			score+=5;
			froggerClient.getGamePanel().updateScore(score);
			froggerClient.send("fivehour");
			froggerClient.getGamePanel().printFive(true);
		}else if(item.equals("cake")){
			score+=50;
			froggerClient.getGamePanel().updateScore(score);
			spawnDrone();
			spawnDrone();
			spawnDrone();
			spawnDrone();
			spawnDrone();
			spawnDrone();
			spawnDrone();
			spawnDrone();
			spawnDrone();
			froggerClient.send("cakeisalie");
			froggerClient.getGamePanel().printCake(true);
		}
	}
	
	public void movePlayerRight(){
		if(playerLocX != 7 && isStunned!=true){
			if(board[playerLocX+1][playerLocY].isPad()){
				if(board[playerLocX+1][playerLocY].getGameItem() != null){
					String item = board[playerLocX+1][playerLocY].getGameItem().getItemString();
					updatePlayer(item);
					board[playerLocX+1][playerLocY].removeItem();	
				}
				playerLocX = playerLocX + 1;
				gameGui.moveRight(); 
			}
		}
	}
	
	public void movePlayerLeft(){
		if(playerLocX != 0 && isStunned!=true){
			if(board[playerLocX-1][playerLocY].isPad()){
				if(board[playerLocX-1][playerLocY].getGameItem() != null){
					String item = board[playerLocX-1][playerLocY].getGameItem().getItemString();
					updatePlayer(item);
					board[playerLocX-1][playerLocY].removeItem();	
				}
				playerLocX = playerLocX - 1;
				gameGui.moveLeft(); 
			}
		}
	}
	
	public void movePlayerUp(){
		if(playerLocY != 0 && isStunned!=true){
			if(board[playerLocX][playerLocY-1].isPad()){
				if(board[playerLocX][playerLocY-1].getGameItem() != null){
					String item = board[playerLocX][playerLocY-1].getGameItem().getItemString();
					updatePlayer(item);
					board[playerLocX][playerLocY-1].removeItem();
				}
				playerLocY = playerLocY - 1;
				gameGui.moveUp(); 
			}
		}
	}
	
	public void movePlayerDown(){
		if(playerLocY != 7 && isStunned!=true){
			if(board[playerLocX][playerLocY+1].isPad()){
				if(board[playerLocX][playerLocY+1].getGameItem() != null){
					String item = board[playerLocX][playerLocY+1].getGameItem().getItemString();
					updatePlayer(item);
					board[playerLocX][playerLocY+1].removeItem();
				}
				playerLocY = playerLocY + 1;
				gameGui.moveDown(); 
			}
		}
	}
	
	public void addEvoPoints(int n){
		enemiesEaten = enemiesEaten + 1;
		froggerClient.getGamePanel().updateEnemiesEaten(enemiesEaten);
		int prevEvoLevel = evoLevel;

		evoPoints += n;
		if(evoPoints>=95){
			evoLevel=4;
			
			froggerClient.getGamePanel().updateEvoLevel(4);
			froggerClient.getGamePanel().getGameManager().winGame();
		}
		else if(evoPoints>=55){
			evoLevel=3;
			froggerClient.getGamePanel().updateEvoLevel(3);
			froggerClient.getGamePanel().changeMyPic(Constants.STAGE3UPIMG);
		}
		else if(evoPoints>=30){
			evoLevel=2;
			froggerClient.getGamePanel().updateEvoLevel(2);
			froggerClient.getGamePanel().changeMyPic(Constants.STAGE2UPIMG);
		}
		else if(evoPoints>=10){
			evoLevel=1;
			froggerClient.getGamePanel().updateEvoLevel(1);
			froggerClient.getGamePanel().changeMyPic(Constants.STAGE1UPIMG);
		}
		else if(evoPoints<10){
			evoLevel=0;
			froggerClient.getGamePanel().updateEvoLevel(0);
			froggerClient.getGamePanel().changeMyPic(Constants.STAGE0UPIMG);
		}
		if(evoLevel != 4){
			gameGui.setCurrState(evoLevel);
			froggerClient.getGamePanel().updateEvoLevel(evoLevel);
		}
		if (prevEvoLevel != evoLevel) {
			health = 4;
			froggerClient.getGamePanel().updateHealth(5);
		}
		froggerClient.send(new EvolveObject(evoLevel));
	}
	
	public void decreaseHealth(){
		health--;
		if(health < 0){
			
			evoLevel--;// = evoLevel - 1;
			health = 4;
			froggerClient.getGamePanel().updateHealth(health);
			if(evoLevel <= 0){
				evoLevel = 0;
				evoPoints = 0;
				froggerClient.getGamePanel().changeMyPic(Constants.STAGE0UPIMG);
			}
			else if(evoLevel == 1){
				evoPoints=10;
				froggerClient.getGamePanel().changeMyPic(Constants.STAGE1UPIMG);
			}
			else if(evoLevel == 2){
				evoPoints=30;
				froggerClient.getGamePanel().changeMyPic(Constants.STAGE2UPIMG);
			}
			gameGui.setCurrState(evoLevel);
		}
		froggerClient.send(new EvolveObject(evoLevel));
		froggerClient.getGamePanel().updateHealth(health);
		froggerClient.getGamePanel().updateEvoLevel(evoLevel);
	}
	
	public Node randPad(){
		int size = padNodes.size(); 
		int choose = rand.nextInt()%size;
		if(choose < 0){
			choose += size-1; 
		}
		return padNodes.get(choose);
	}
	
	public void randSprout(){
		Node arg = randPad(); 
		int xLoc = arg.getX();
		int yLoc = arg.getY();
		/**
		int xLoc = 4;
		int yLoc = 4;
		**/
		String itemType = "candy";
		int choose = Math.abs(rand.nextInt()%4);
		if(choose == 0){
			itemType = "candy"; 
		}else if(choose == 1){
			itemType = "fivehour";
		}else if(choose == 2){
			itemType = "cake";
		}else if(choose ==3){
			itemType = "cake";
		}else itemType = "cookie";
		board[xLoc][yLoc].setItem(itemType);
		String itemName = board[xLoc][yLoc].getGameItem().getItemString();
		gameGui.putItem(xLoc,yLoc,itemName);
	}
	
	public void spawnEnemy(){
		int pos=rand.nextInt()%edgeNodes.size();//spot in vector
		if (pos < 0) {
			pos += edgeNodes.size();
		}
		Node n=edgeNodes.get(pos);
		
		int level=rand.nextInt(evoLevel + 2);
//		if (level < 0){
//			level = evoLevel + 1;
//		}
		
		System.out.println("spawning enemy, evoLevel = " + evoLevel);
		System.out.println("enemy level being spawned = " + level);
		Enemy en=new Enemy(n,level,this);
	}
	
	public void spawnDrone(){
		int pos=rand.nextInt()%edgeNodes.size();//spot in vector
		if (pos < 0) {
			pos += edgeNodes.size();
		}
		Node n=edgeNodes.get(pos);
		Enemy en = new Enemy(n, 4, this);
	}
	
	public void spawnHand(){
		int pos=rand.nextInt()%edgeNodes.size();//spot in vector
		if (pos < 0) {
			pos += edgeNodes.size();
		}
		Node n=edgeNodes.get(pos);
		
		Enemy en = new Enemy(n, 3, this);
	}
	
	public void fiveHour(){
		for(int i=0; i<10; i++){
			int nextEnemy = rand.nextInt()%2;
			if(nextEnemy == 0){
				spawnDrone();
			}
			else{
				spawnHand();
			}
		}
	}
	
	// --- [ get player stats ] --- //
		public int getScore(){
			return score;
		}
		public int getNumEaten(){
			return enemiesEaten;
		}

	public void resetBoard(){
		itemsEaten = 0; 
		enemiesEaten = 0;
		score = 0;
		playerLocX = 4; 
		playerLocY = 4;
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				board[i][j].removeItem();
			}
		}
		froggerClient.getGamePanel().updateScore(0);
		froggerClient.getGamePanel().updateHealth(4);
		froggerClient.getGamePanel().updateEnemiesEaten(0);
		froggerClient.getGamePanel().updateEvoLevel(0);
		froggerClient.getGamePanel().updateTime(0);

		gameGui.resetGui(); 
	}

	
}
