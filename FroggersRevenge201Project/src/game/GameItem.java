//GAMEITEM 
package game;

import java.awt.Image;

import game.Node;


public abstract class GameItem {
	protected String itemString; 
	//set node the item is on
	//take effect on player/opponent
	public abstract String getItemString();
}