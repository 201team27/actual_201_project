package game;

public class EnemySpawnThread extends Thread {
	
	// this will be called in the GameManager's start function (not written yet)
	// it will also be saved as a variable so that it can be interrupted when the game ends
	
	private GameBoard gameBoard;
	
	public EnemySpawnThread(GameBoard gameBoard) {
		
		//save gameboard
		this.gameBoard=gameBoard;
	}
	
	public void run() {
		try {
			while(true) {
				// call random spawn function on gameboard
				gameBoard.spawnEnemy();
				// wait for 5 secs
				Thread.sleep(2000);
			}
		} catch (InterruptedException ie) {
			System.out.println("Stopped Spawning Enemies!");
		}
	}

}
