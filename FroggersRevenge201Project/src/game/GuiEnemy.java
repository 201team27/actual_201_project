package game;

import java.awt.Image;

import javax.swing.ImageIcon;

public class GuiEnemy {
	private int x,y; 
	private Image enemyImg;
	
	public GuiEnemy(){
		
	}
	
	public GuiEnemy(ImageIcon image, int x, int y) {
		this.x = x;
		this.y = y;
		enemyImg = image.getImage();
	}
	
	public void setImage(ImageIcon image){
		enemyImg = image.getImage();
	}
	
	public void setX(int xCord){
		x = xCord; 
	}
	
	public void setY(int yCord){
		y = yCord; 
	}
	
	public Image getImage() {
		return enemyImg;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
}
