//GAMEMANAGER
package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Timestamp;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import communication.EndGameObject;
import communication.PlayerInfo;
import gameclient.ClientPanel;
import gameclient.GameGui;
import gameclient.GamePanel;
import library.Constants;
import networking.FroggerClient;

public class GameManager implements Runnable {
	
	private Timestamp startTime;
	private ClientPanel clientPanel; 
	private Vector<Integer> totalTime;
	
	//private GameSetup game; 
	
	private Thread animator;
	private boolean running;
	
	private Lock animationLock;
	
	private FroggerClient froggerClient;
	
	private GameBoard gameBoard;
	
	private GameGui gameGui;
	
	private GamePanel gamePanel;
	private ItemSpawnThread itemThread; 
	private EnemySpawnThread enemySpawnThread;
	private RepaintThread repaintThread;
	
	public GameManager(FroggerClient froggerClient, GameBoard gameBoard, GamePanel gamePanel) {
		this.froggerClient = froggerClient;
		animationLock = new ReentrantLock(); 
		running = false; 
		this.gameBoard = gameBoard;
		gameGui = gameBoard.getGameGui();
		this.gamePanel = gamePanel;
		itemThread = new ItemSpawnThread(gameBoard); 
		enemySpawnThread = new EnemySpawnThread(gameBoard);
		repaintThread = new RepaintThread(gameGui, this.gamePanel);
		
		//set up blank time keeper for now
		//updates when you start and stop timer
		totalTime = new Vector<Integer>();
		totalTime.add(0);
		totalTime.add(0);
		totalTime.add(0);
		//gamePanel.updateTime(0);
		//game = new GameSetup(); 
		addListeners();
	}
	
	public void startGame(){
		itemThread.start();
		enemySpawnThread.start();
		repaintThread.start();
		gamePanel.changeMyPic(Constants.STAGE0UPIMG);
		gamePanel.changeTheirPic(Constants.STAGE0UPIMG);
		
		startTimer();
	}
	
	public void startTimer(){
		startTime = new Timestamp(System.currentTimeMillis());
	}
	
	public Vector<Integer> stopTimer(){
		Timestamp endTime = new Timestamp(System.currentTimeMillis());
		
		  long milliseconds1 = startTime.getTime();
		  long milliseconds2 = endTime.getTime();

		  long diff = milliseconds2 - milliseconds1;
		  int seconds = (int) (diff / 1000) % 60 ;
		  int minutes = (int) ((diff / (1000*60)) % 60);
		  int hours   = (int) ((diff / (1000*60*60)) % 24);
		  
		  Vector<Integer> differences = new Vector<Integer>();
		  differences.add(hours);
		  differences.add(minutes);
		  differences.add(seconds);
		  
		  
		  return differences;
		
	}
	
	
	public void start() {
		startAnimation(); 
	}
	
	public void startAnimation() {
		// Create animator thread 
		if (animator == null || !running ) {
			animator = new Thread(this); 
			animator.start(); 
		}
	}
	
	public void stop() {
		running = false; 
		animator = null; 
	}

	public void run() {
		animationLock.lock();
		long beginTime = System.nanoTime();
		long difference= 0;
		running = true;
		
		while (running) {
			// Update graphics here from Game Setup 
//			if (game != null) {
//				game.update(difference);
//			}
		}
		
	}
	
	
	public void addListeners() {
		gamePanel.addKeyListener(new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_RIGHT ) {
		            //System.out.println("Right released.");
		            gameBoard.movePlayerRight(); 
		        } 
		        else if (e.getKeyCode() == KeyEvent.VK_LEFT ) {
		            //System.out.println("Left released.");
		            gameBoard.movePlayerLeft(); 
		        } 
		        else if (e.getKeyCode() == KeyEvent.VK_UP ) {
		            //System.out.println("Up released.");
		            gameBoard.movePlayerUp(); 
		        } 
		        else if (e.getKeyCode() == KeyEvent.VK_DOWN ) {
		            //System.out.println("Down released.");
		            gameBoard.movePlayerDown(); 
		        }
		        else{
		            System.out.println("Key released: " + e.getKeyChar());
		        }
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
//				
			}
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	//---[ Handle win/lose situation ]---//
		//interrupt the threads
		//set threads equal to new threads
		//call dialog open on clientPanel
		//update database
		//called when first person wins or loses.
		public void winGame(){
			stopGame();
			updateInfoExceptWins();
			froggerClient.getPlayerInfo().incrementWins();
			EndGameObject eg = new EndGameObject(froggerClient.getPlayerInfo(), true);
			froggerClient.send(eg);
			gamePanel.showWinGame();
			gameBoard.resetBoard();
			
		}
		
		public void loseGame(){
			stopGame();
			updateInfoExceptWins();
			EndGameObject eg = new EndGameObject(froggerClient.getPlayerInfo(), false);
			froggerClient.send(eg);
			gamePanel.showLoseGame();
			gameBoard.resetBoard();
		}
		
		private void stopGame(){
			totalTime = stopTimer();
			itemThread.interrupt();
			enemySpawnThread.interrupt();
			repaintThread.interrupt();
			itemThread = new ItemSpawnThread(gameBoard);
			enemySpawnThread = new EnemySpawnThread(gameBoard);
			repaintThread = new RepaintThread(gameGui, gamePanel);
		}
		
		private void updateInfoExceptWins(){
			PlayerInfo pi = froggerClient.getPlayerInfo();
			pi.setTopScore(gameBoard.getScore());
			pi.setBestTime(totalTime);
			pi.setWorstTime(totalTime);
			pi.addNumEaten(gameBoard.getNumEaten());
			pi.incrementGamesPlayed();
		}

}