package game;

public class GameFrog {
	private int level=0;
	private int health=0;
	//increase level
	public void increaseLevel(){
		if (level!=4) level=level+1;
	}
	//increase health
	public void increaseHealth(){
		health=health+1;
	}
	//decrease health
	public void decreaseHealth(){
		if (health>=0) health=health-1;
	}
}
