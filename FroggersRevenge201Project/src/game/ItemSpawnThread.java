package game;

public class ItemSpawnThread extends Thread {
	
	// this will be called in the GameManager's start function (not written yet)
		// it will also be saved as a variable so that it can be interrupted when the game ends
		
		private GameBoard gameBoard;
		
		public ItemSpawnThread(GameBoard gameBoard) {
			
			//save gameboard
			this.gameBoard=gameBoard;
		}
		
		public void run() {
			try {
				while(true) {
					// call random spawn function on gameboard
					gameBoard.randSprout();
					// wait for 5 secs
					Thread.sleep(25000);
				}
			} catch (InterruptedException ie) {
				System.out.println("Stopped Spawning Items!");
			}
		}

}
