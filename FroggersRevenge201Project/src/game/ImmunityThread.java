package game;

public class ImmunityThread extends Thread {
	
	private GameBoard gb;
	private int secsOfEffect;
	
	public ImmunityThread(GameBoard gb, int secsOfEffect) {
		this.gb = gb;
		this.secsOfEffect = secsOfEffect;
	}
	
	public void run() {
		try {
			gb.setImmunity(true);
			Long startTime = System.currentTimeMillis();
			gb.getGameGui().setImmune();
			while (1000*secsOfEffect > (System.currentTimeMillis() - startTime)) {
				if(gb.getGameGui().getType() < 2){
					gb.getGameGui().setImmune();
				}
				Thread.sleep(100);	
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			gb.setImmunity(false);
			if(gb.getGameGui().getType() == 2){
				gb.getGameGui().setNormal();
			}
		}
	}
}
