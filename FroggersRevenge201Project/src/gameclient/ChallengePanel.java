package gameclient;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import communication.ChallengeResponseObject;
import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class ChallengePanel extends JPanel implements Runnable {

	private static final long serialVersionUID = -3690672762249850648L;
	
	private FroggerClient froggerClient;
	private String challenger;
	private long startTime;
	private JLabel timeLabel;
	private int currentTime;
	private boolean selected;
	private boolean choice;
	private JButton acceptButton;
	private JButton rejectButton;
	private JDialog jd;
	private GridBagConstraints gbc;
	private ImageIcon bgImage; 
	
	public ChallengePanel(String challenger, FroggerClient froggerClient, JDialog jd) {
		super();
		this.froggerClient = froggerClient;
		this.challenger = challenger;
		this.jd = jd;
		selected = false;
		choice = false;
		currentTime = 30;
		
		bgImage = Constants.POLKADOTIMG;
		
		setLayout(new GridBagLayout());
		setSize(new Dimension(600,300));
		
		//JPanel upper = new JPanel(new GridBagLayout());
		
		gbc = new GridBagConstraints();
		//chatHeading.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		
		// make and add name
		JLabel nameLabel = new JLabel(challenger);
		nameLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		//gbc.fill = GridBagConstraints.BOTH;
		add(nameLabel, gbc);
		
		// make and add first line
		JLabel sentence = new JLabel("has challenged you!");
		sentence.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 24));
		gbc.gridy = 1;
		add(sentence, gbc);
		
		//add(upper, BorderLayout.NORTH);
		// make and add
		timeLabel = new JLabel(((Integer)currentTime).toString() + " secs to respond!");
		timeLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		gbc.gridy = 2;
		add(timeLabel);
		
		//JPanel lower = new JPanel(new GridBagLayout());
		
		acceptButton = new JButton();
		Image loginImg11 = Constants.ACCEPTBTN2.getImage().getScaledInstance(260, 80, Image.SCALE_SMOOTH);
		acceptButton.setIcon(new ImageIcon(loginImg11));// overwrite paint and depressed icon
		Image loginImg21 = Constants.ACCEPTBTN1.getImage().getScaledInstance(260, 80, Image.SCALE_SMOOTH);
		acceptButton.setPressedIcon(new ImageIcon(loginImg21));
		acceptButton.setOpaque(false);
		acceptButton.setContentAreaFilled(false);
		acceptButton.setBorder(BorderFactory.createEmptyBorder());
		acceptButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				choice = true;
				selected = true;
			}
		});
		gbc.gridy = 3;
		gbc.gridx = 0;
		gbc.gridwidth = 1;
		add(acceptButton, gbc);
		
		rejectButton = new JButton();
		Image loginImg1 = Constants.DECLINEBTN2.getImage().getScaledInstance(260, 80, Image.SCALE_SMOOTH);
		rejectButton.setIcon(new ImageIcon(loginImg1));// overwrite paint and depressed icon
		Image loginImg2 = Constants.DECLINEBTN1.getImage().getScaledInstance(260, 80, Image.SCALE_SMOOTH);
		rejectButton.setPressedIcon(new ImageIcon(loginImg2));
		rejectButton.setOpaque(false);
		rejectButton.setContentAreaFilled(false);
		rejectButton.setBorder(BorderFactory.createEmptyBorder());
		rejectButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				choice = false;
				selected = true;
			}
		});
		gbc.gridx = 1;
		add(rejectButton, gbc);
		//add(lower, BorderLayout.SOUTH);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		
	}

	@Override
	public void run() {
		try {
			startTime = System.currentTimeMillis();
			System.out.println("startTime = " + startTime);
			while (currentTime > 0 && !selected) {
				Thread.sleep(250);
				currentTime = 30 - (int)(System.currentTimeMillis() - startTime)/1000;
				//JLabel newTime = new JLabel(((Integer)currentTime).toString());
				remove(timeLabel);
				timeLabel = new JLabel(((Integer)currentTime).toString() + " secs to respond!");
				timeLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
				add(timeLabel, gbc);
				repaint();
				revalidate();
			}
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			
			ChallengeResponseObject cro = new ChallengeResponseObject();
			cro.accepted = choice;
			cro.challenged = froggerClient.getPlayerInfo().getUsername();
			cro.challenger = challenger;
			
			froggerClient.send(cro);			
			jd.dispose();
		}
	}
	
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	        g.drawImage(bgImage.getImage(), 0, 0,getWidth(),getHeight(),this);
	}
}
