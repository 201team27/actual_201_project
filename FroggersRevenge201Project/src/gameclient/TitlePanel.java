package gameclient;

//Reena
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

import library.Constants;


public class TitlePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public TitlePanel(JButton startButton) {
		super();
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.insets = new Insets(340, 380, 20, 20);
		
		add(startButton, gbc);
	}
	
	//@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(new Color(255,255,255));
		g.fillRect(0, 0, getWidth(), getHeight());
		// Graphics2D g2d = (Graphics2D) g.create();
		
		g.drawImage(Constants.TITLEBACKGROUND.getImage(), 0, 0, getWidth(), getHeight(), null);
	}
}
