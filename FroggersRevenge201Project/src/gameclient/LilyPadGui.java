//LILYPADGUI 
package gameclient; 
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import library.Constants;

public class LilyPadGui extends JPanel{
	private static final long serialVersionUID = 3609831945869059312L;
	private ImageIcon bgImage;
	
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	        g.drawImage(bgImage.getImage(), 0, 0,getWidth(),getHeight(),this);
	}
	
	//default constructor, basic lily 
	public LilyPadGui(){
    	setLayout(new BorderLayout());
    	//try{
    	bgImage = Constants.BASICLILYIMG;//ImageIO.read(new File("resources/basiclily.png"));
//		}
//		catch(IOException e) {
//			e.printStackTrace();
//		}
    	setSize(10,10);
    	setVisible(true);
//    	repaint();
//    	validate();
	}
	
	//special constructor
	//@param String stage is the stage of the frog we want 
	//@param String dir is the direction of the frog we want
    public LilyPadGui(int stage, String dir) {
    	setLayout(new BorderLayout());
    	//stage 0 images
    	if(stage == 0){
    		if(dir.equals("up")){
    			bgImage = Constants.STAGE0UPIMG;
    		}
    		if(dir.equals("right")){
    			bgImage = Constants.STAGE0RIGHTIMG;
    		}
    		if(dir.equals("down")){
    			bgImage = Constants.STAGE0DOWNIMG;
    		}
    		if(dir.equals("left")){
    			bgImage = Constants.STAGE0LEFTIMG;
    		}
    	}
    	//stage 1 images
    	if(stage == 1){
    		if(dir.equals("up")){
    			bgImage = Constants.STAGE1UPIMG;
    		}
    		if(dir.equals("right")){
    			bgImage = Constants.STAGE1RIGHTIMG;
    		}
    		if(dir.equals("down")){
    			bgImage = Constants.STAGE1DOWNIMG;
    		}
    		if(dir.equals("left")){
    			bgImage = Constants.STAGE1LEFTIMG;
    		}
    	}
    	//stage 2 images
    	if(stage == 2){
    		if(dir.equals("up")){
    			bgImage = Constants.STAGE2UPIMG;
    		}
    		if(dir.equals("right")){
    			bgImage = Constants.STAGE2RIGHTIMG;
    		}
    		if(dir.equals("down")){
    			bgImage = Constants.STAGE2DOWNIMG;
    		}
    		if(dir.equals("left")){
    			bgImage = Constants.STAGE2LEFTIMG;
    		}
    	}
    	//stage 3 images
    	if(stage == 3){
    		if(dir.equals("up")){
    			bgImage = Constants.STAGE3UPIMG;
    		}
    		if(dir.equals("right")){
    			bgImage = Constants.STAGE3RIGHTIMG;
    		}
    		if(dir.equals("down")){
    			bgImage = Constants.STAGE3DOWNIMG;
    		}
    		if(dir.equals("left")){
    			bgImage = Constants.STAGE3LEFTIMG;
    		}
    	}
    	setSize(10,10);
    	setVisible(true);
//    	repaint();
//    	validate();
    }
    
    public void changeDefault(){
    	bgImage = Constants.BASICLILYIMG;
//    	repaint();
//    	validate();
    	//System.out.println("cries");
    }
    
    public void change(int stage, String dir, int type){
    if(stage == 0){
		if(dir.equals("up")){
			if(type == 0){
				bgImage = Constants.STAGE0UPIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE0UPIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE0UPIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE0UPIMG;
			}
		}
		if(dir.equals("right")){
			if(type == 0){
				bgImage = Constants.STAGE0RIGHTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE0RIGHTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE0RIGHTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE0RIGHTIMG;
			}
		}
		if(dir.equals("down")){
			if(type == 0){
				bgImage = Constants.STAGE0DOWNIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE0DOWNIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE0DOWNIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE0DOWNIMG;
			}
		}
		if(dir.equals("left")){
			if(type == 0){
				bgImage = Constants.STAGE0LEFTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE0LEFTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE0LEFTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE0LEFTIMG;
			}
		}
    }
	//stage 1 images
    if(stage == 1){
		if(dir.equals("up")){
			if(type == 0){
				bgImage = Constants.STAGE1UPIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE1UPIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE1UPIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE1UPIMG;
			}
		}
		if(dir.equals("right")){
			if(type == 0){
				bgImage = Constants.STAGE1RIGHTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE1RIGHTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE1RIGHTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE1RIGHTIMG;
			}
		}
		if(dir.equals("down")){
			if(type == 0){
				bgImage = Constants.STAGE1DOWNIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE1DOWNIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE1DOWNIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE1DOWNIMG;
			}
		}
		if(dir.equals("left")){
			if(type == 0){
				bgImage = Constants.STAGE1LEFTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE1LEFTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE1LEFTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE1LEFTIMG;
			}
		}
    }
	//stage 2 images
    if(stage == 2){
		if(dir.equals("up")){
			if(type == 0){
				bgImage = Constants.STAGE2UPIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE2UPIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE2UPIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE2UPIMG;
			}
		}
		if(dir.equals("right")){
			if(type == 0){
				bgImage = Constants.STAGE2RIGHTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE2RIGHTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE2RIGHTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE2RIGHTIMG;
			}
		}
		if(dir.equals("down")){
			if(type == 0){
				bgImage = Constants.STAGE2DOWNIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE2DOWNIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE2DOWNIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE2DOWNIMG;
			}
		}
		if(dir.equals("left")){
			if(type == 0){
				bgImage = Constants.STAGE2LEFTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE2LEFTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE2LEFTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE2LEFTIMG;
			}
		}
    }
	//stage 3 images
    if(stage == 3){
		if(dir.equals("up")){
			if(type == 0){
				bgImage = Constants.STAGE3UPIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE3UPIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE3UPIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE3UPIMG;
			}
		}
		if(dir.equals("right")){
			if(type == 0){
				bgImage = Constants.STAGE3RIGHTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE3RIGHTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE3RIGHTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE3RIGHTIMG;
			}
		}
		if(dir.equals("down")){
			if(type == 0){
				bgImage = Constants.STAGE3DOWNIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE3DOWNIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE3DOWNIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE3DOWNIMG;
			}
		}
		if(dir.equals("left")){
			if(type == 0){
				bgImage = Constants.STAGE3LEFTIMG;
			}else if(type == 1){
				bgImage = Constants.REDSTAGE3LEFTIMG;
			}else if(type == 2){
				bgImage = Constants.IMMUNESTAGE3LEFTIMG;
			}else if(type == 3){
				bgImage = Constants.GODSTAGE3LEFTIMG;
			}
		}
    }
	//repaint();
	//validate();
}
    
    public void putItem(String item){
    	if(item.equals("cake")){
    		bgImage = Constants.CUPCAKEIMG;
    	}
    	else if(item.equals("candy")){
    		bgImage = Constants.CANDYIMG;
    	}
    	else if(item.equals("fivehour")){
    		bgImage = Constants.FIVEHOURIMG;
    	}
    	else if(item.equals("blank")){
    		bgImage = Constants.BLANKIMG;
    	}else if(item.equals("cookie")){
    		bgImage = Constants.COOKIEIMG;
    	
    	}else {
    		System.out.println("set type DNE");
    	}
    	//repaint();
    	//validate();
    }
    
    public void paint(Graphics g, GameGui gg, int x, int y) {
    	//super.paint(g);
    	g.drawImage(bgImage.getImage(), x, y, gg.getWidth() / 8, gg.getHeight()/8, null);
    	
    }
    
    
}