package gameclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import communication.GameChatObject;
import game.GameBoard;
import game.GameManager;
import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class GamePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	// chat stuff
	private JTextArea chatField;
	private JTextPane chatPane;
	private JButton sendButton;
	private SimpleAttributeSet white, black, red, blue, green;
	// keeps chat text consistent
	private Lock chatLock;
	
	// mutable statistic fields
	private JLabel scoreLabel, healthLabel, eatenLabel, evoLevelLabel, timeLabel;
	private JButton rageQuitButton;
	
	// supposed to be your icon, may change from JLabel depending on what we do with this
	private JLabel yourStatusLabel;
	
	// supposed to be opponent's icon, may change from JLabel depending on what we do with this
	private JLabel opponentStatusLabel;
	
	// will hold stats bar and the actual board
	// goes in the center of the main layout of 'this'
	private JPanel boardStatsPanel;
	
	// NORTH in boardStatsPanel
	// holds all stat info on top of screen
	private JPanel statsPanel;
	private JPanel scoreBox, healthBox, eatenBox, evoLevelBox, timeBox;
	
	// goes in EAST of 'this'
	// holds everything on right side of gui
	private JPanel rightPanel;
	
	// goes in NORTH of rightPanel
	// contains player status' and notification heading
	private JPanel playerStatusPanel;
	
	// goes in SOUTH of rightPanel
	// contains chatField (CENTER), sendButton (EAST)
	private JPanel textSendPanel;
	
	private GameManager gameManager;
	
	private GameBoard gameBoard;
	
	private GameGui gameGui;
	
	private FroggerClient froggerClient;
	
	private GamePanel thisGamePanel;
	private static final int STATUS_SIZE = 100;
	
	public GamePanel(FroggerClient froggerClient, JMenuItem endGameItem) {
		super();
		this.froggerClient = froggerClient;
		
		initializeComponents();
		createGUI();
		addEvents();
		gameGui.requestFocusInWindow();
		this.setFocusable(true);
		
		thisGamePanel = this;
		
		//end game in the menu item
		endGameItem.addActionListener(new ActionListener(){
			//override
			public void actionPerformed(ActionEvent ae){
				gameManager.loseGame();
			}
		});
	}
	
	public void initializeComponents() {
		chatLock = new ReentrantLock();
		gameGui= new GameGui();
		gameBoard = new GameBoard(gameGui, froggerClient);
		gameManager = new GameManager(froggerClient, gameBoard, this);
		chatField = new JTextArea();
		chatPane = new JTextPane();
		sendButton = new JButton();
		Image chatImg1 = Constants.SENDBUTTONIMG1.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		sendButton.setIcon(new ImageIcon(chatImg1));// overwrite paint and depressed icon
		Image chatImg2 = Constants.SENDBUTTONIMG2.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		sendButton.setPressedIcon(new ImageIcon(chatImg2));
		sendButton.setOpaque(false);
		sendButton.setContentAreaFilled(false);
		sendButton.setBorder(BorderFactory.createEmptyBorder());
		white = new SimpleAttributeSet();
		StyleConstants.setForeground(white, Color.WHITE);
		black = new SimpleAttributeSet();
		StyleConstants.setForeground(black, Color.BLACK);
		red = new SimpleAttributeSet();
		StyleConstants.setForeground(red, Color.RED);
		blue = new SimpleAttributeSet();
		StyleConstants.setForeground(blue, Color.BLUE);
		green = new SimpleAttributeSet();
		StyleConstants.setForeground(green, Color.GREEN);
		
		// score
		scoreLabel = new JLabel("0");
		scoreLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		scoreLabel.setOpaque(false);
		scoreLabel.setBorder(BorderFactory.createEmptyBorder());
		healthLabel = new JLabel("*****");
		healthLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		healthLabel.setOpaque(false);
		healthLabel.setBorder(BorderFactory.createEmptyBorder());
		// #eaten
		eatenLabel = new JLabel("0");
		eatenLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		eatenLabel.setOpaque(false);
		eatenLabel.setBorder(BorderFactory.createEmptyBorder());
		
		//evo level
		evoLevelLabel = new JLabel("0");
		evoLevelLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		evoLevelLabel.setOpaque(false);
		evoLevelLabel.setBorder(BorderFactory.createEmptyBorder());
		timeLabel = new JLabel("0:00");
		timeLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		timeLabel.setOpaque(false);
		timeLabel.setBorder(BorderFactory.createEmptyBorder());
		rageQuitButton = new JButton();
		Image loginImg1 = Constants.ENDGAMEBTN1.getImage().getScaledInstance(110, 48, Image.SCALE_SMOOTH);
		
		rageQuitButton.setIcon(new ImageIcon(loginImg1));// overwrite paint and depressed icon
		Image loginImg2 = Constants.ENDGAMEBTN2.getImage().getScaledInstance(110, 48, Image.SCALE_SMOOTH);
		rageQuitButton.setPressedIcon(new ImageIcon(loginImg2));
		rageQuitButton.setOpaque(false);
		rageQuitButton.setContentAreaFilled(false);
		rageQuitButton.setBorder(BorderFactory.createEmptyBorder());
		
		
		Image mystart = Constants.STAGE0UPIMG.getImage().getScaledInstance(STATUS_SIZE, STATUS_SIZE, Image.SCALE_SMOOTH);
		Image theirstart = Constants.STAGE0UPIMG.getImage().getScaledInstance(STATUS_SIZE, STATUS_SIZE, Image.SCALE_SMOOTH);
		
		yourStatusLabel = new JLabel(new ImageIcon(mystart));
		yourStatusLabel.setOpaque(false);
		yourStatusLabel.setBorder(BorderFactory.createEmptyBorder(5,30,5,5));
		opponentStatusLabel = new JLabel(new ImageIcon(theirstart));
		opponentStatusLabel.setOpaque(false);
		opponentStatusLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,30));
		
		boardStatsPanel = new JPanel(new BorderLayout());
		boardStatsPanel.setOpaque(false);
		boardStatsPanel.setBorder(BorderFactory.createEmptyBorder());
		statsPanel = new JPanel();
		statsPanel.setOpaque(false);
		//statsPanel.setBorder(BorderFactory.createEmptyBorder());
		rightPanel = new JPanel(new BorderLayout());
		rightPanel.setOpaque(false);
		playerStatusPanel = new JPanel(new BorderLayout());
		playerStatusPanel.setOpaque(false);
		//playerStatusPanel.setBorder(BorderFactory.createEmptyBorder());
		textSendPanel = new JPanel(new BorderLayout());
		textSendPanel.setOpaque(false);
		textSendPanel.setBorder(BorderFactory.createEmptyBorder());
		
		scoreBox = new JPanel();
		scoreBox.setOpaque(false);
		scoreBox.setBorder(BorderFactory.createEmptyBorder());
		healthBox = new JPanel();
		healthBox.setOpaque(false);
		healthBox.setBorder(BorderFactory.createEmptyBorder());
		eatenBox = new JPanel();
		eatenBox.setOpaque(false);
		eatenBox.setBorder(BorderFactory.createEmptyBorder());
		evoLevelBox = new JPanel();
		evoLevelBox.setOpaque(false);
		evoLevelBox.setBorder(BorderFactory.createEmptyBorder());
		timeBox = new JPanel();
		timeBox.setOpaque(false);
		timeBox.setBorder(BorderFactory.createEmptyBorder());
		
		
		
	}
	
	public void createGUI() {
		setLayout(new BorderLayout());
		
		// make all the stuff look right here
		// yeah, gonna be a lot
		
		rightPanel.setMaximumSize(new Dimension(300,400));
		playerStatusPanel.setPreferredSize(new Dimension(300,200));
		
		// create stats layout
		statsPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.gridwidth=1;
		gbc.fill=GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(3, 3, 3, 0);
		JLabel s = new JLabel("Score: ");
		s.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		s.setOpaque(false);
		s.setBorder(BorderFactory.createEmptyBorder());
		statsPanel.add(s, gbc);
		
		gbc.gridx=1;
		gbc.insets = new Insets(3, 0, 3, 3);
		scoreBox.add(scoreLabel);
		statsPanel.add(scoreBox, gbc);
		
		gbc.gridx=2;
		gbc.insets = new Insets(3, 3, 3, 0);
		JLabel h = new JLabel("Health: ");
		h.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		h.setOpaque(false);
		h.setBorder(BorderFactory.createEmptyBorder());
		statsPanel.add(h, gbc);
		
		gbc.gridx=3;
		gbc.insets = new Insets(3, 0, 3, 3);
		healthBox.add(healthLabel);
		statsPanel.add(healthBox, gbc);
		
		gbc.gridx=4;
		gbc.insets = new Insets(3, 3, 3, 0);
		JLabel e = new JLabel("# Eaten: ");
		e.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		e.setOpaque(false);
		e.setBorder(BorderFactory.createEmptyBorder());
		statsPanel.add(e, gbc);
		
		gbc.gridx=5;
		gbc.insets = new Insets(3, 0, 3, 3);
		eatenBox.add(eatenLabel);
		statsPanel.add(eatenBox, gbc);
		
		gbc.gridx=6;
		gbc.insets = new Insets(3, 3, 3, 0);
		JLabel el = new JLabel("Evo Level: ");
		el.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		el.setOpaque(false);
		el.setBorder(BorderFactory.createEmptyBorder());
		statsPanel.add(el, gbc);
		
		gbc.gridx=7;
		gbc.insets = new Insets(3, 0, 3, 3);
		evoLevelBox.add(evoLevelLabel);
		statsPanel.add(evoLevelBox, gbc);
		
		gbc.gridx=8;
		gbc.insets = new Insets(3, 3, 3, 0);
		JLabel t = new JLabel("Time: ");
		t.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		t.setOpaque(false);
		t.setBorder(BorderFactory.createEmptyBorder());
		statsPanel.add(t, gbc);
		
		gbc.gridx=9;
		gbc.insets = new Insets(3, 0, 3, 3);
		timeBox.add(timeLabel);
		statsPanel.add(timeBox, gbc);
		
		gbc.gridx=10;
		statsPanel.add(rageQuitButton, gbc);
		
		// build gameboard and stats panel, then add to main layout
		boardStatsPanel.add(gameGui);
		gameGui.requestFocusInWindow(); 
		boardStatsPanel.add(statsPanel, BorderLayout.NORTH);
		add(boardStatsPanel);
		
		// NOTE: need to figure out how this gets put together
		// player status stuff
		JLabel youOpLabel = new JLabel("    You     Opponent");
		youOpLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		youOpLabel.setOpaque(false);
		youOpLabel.setBorder(BorderFactory.createEmptyBorder());
		playerStatusPanel.add(youOpLabel, BorderLayout.NORTH);
		playerStatusPanel.add(yourStatusLabel, BorderLayout.WEST);
		playerStatusPanel.add(opponentStatusLabel, BorderLayout.EAST);
		JLabel notificationsLabel = new JLabel("Notifications:");
		notificationsLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		notificationsLabel.setOpaque(false);
		notificationsLabel.setBorder(BorderFactory.createEmptyBorder());
		playerStatusPanel.add(notificationsLabel, BorderLayout.SOUTH);
		
		rightPanel.add(playerStatusPanel, BorderLayout.NORTH);
		
		// set text pane characteristics (font, size, etc)
		chatPane.setEditable(false);
		chatPane.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 24));
		
		JScrollPane spChat = new JScrollPane(chatPane);
		
		//sp.setBounds(10,60,780,500);
		spChat.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spChat.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		rightPanel.add(spChat);
		
		sendButton.setPreferredSize(new Dimension(100, 100));		
		
		chatField.setLineWrap(true);
		chatField.setWrapStyleWord(true);
		chatField.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		JScrollPane spField = new JScrollPane(chatField);
		spField.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spField.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		textSendPanel.add(spField);
		textSendPanel.add(sendButton, BorderLayout.EAST);
		textSendPanel.setOpaque(false);
		rightPanel.add(textSendPanel, BorderLayout.SOUTH);
		
		
		add(rightPanel, BorderLayout.EAST);
	}
	
	public void addEvents() {
		
		rageQuitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				 //Dialog box to be spruced up with graphics 
				JDialog dialog = new JDialog();
				JPanel popup = new JPanel(new GridBagLayout()); 
				JButton yesButton = new JButton();
				JButton noButton = new JButton(); 
				
				Image im1 = Constants.ACCEPTBTN1.getImage().getScaledInstance(140, 80, Image.SCALE_SMOOTH);
				yesButton.setIcon(new ImageIcon(im1));// overwrite paint and depressed icon
				Image im2 = Constants.ACCEPTBTN2.getImage().getScaledInstance(140, 80, Image.SCALE_SMOOTH);
				yesButton.setPressedIcon(new ImageIcon(im2));
				yesButton.setOpaque(false);
				yesButton.setContentAreaFilled(false);
				yesButton.setBorder(BorderFactory.createEmptyBorder());
				
				Image im3 = Constants.DECLINEBTN1.getImage().getScaledInstance(140, 80, Image.SCALE_SMOOTH);
				noButton.setIcon(new ImageIcon(im3));// overwrite paint and depressed icon
				Image im4 = Constants.DECLINEBTN2.getImage().getScaledInstance(140, 80, Image.SCALE_SMOOTH);
				noButton.setPressedIcon(new ImageIcon(im4));
				noButton.setOpaque(false);
				noButton.setContentAreaFilled(false);
				noButton.setBorder(BorderFactory.createEmptyBorder());
				
				dialog.setSize(330,200);
				dialog.setTitle("End Game");
				dialog.setLocationRelativeTo(null);
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = 0;
				gbc.gridwidth = 2;
				
				JLabel question = new JLabel("Are you sure you want to exit?");
				question.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 24));
				question.setOpaque(false);
				question.setBorder(BorderFactory.createEmptyBorder());
				popup.add(question, gbc);
				
				gbc.gridwidth = 1;
				gbc.gridy = 1;
				popup.add(yesButton, gbc); 
				
				gbc.gridx = 1;
				popup.add(noButton, gbc);
				dialog.add(popup); 
				dialog.setVisible(true);
				
				// Confirm exit game 
				yesButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent ae) {
						// Need to add functionality here -- 
						//  killing the game, update stats, place people in lobby
						gameManager.loseGame(); //ends game, updates stats, notifies server
						dialog.setVisible(false);
					}
				});
				
				noButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent ae) {
						dialog.setVisible(false);
					}
				});
			}
		});
		
		sendButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				if (!chatField.getText().equals("")) {
					chat(chatField.getText());
				}
				thisGamePanel.requestFocus();
			}
		});
		
		chatField.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent ke) {
				if(ke.getKeyCode() == KeyEvent.VK_ENTER){
					ke.consume();
					if (!chatField.getText().equals("")) {
						chat(chatField.getText());
					}
					thisGamePanel.requestFocus();
	            }
				
			}
		});
		
		gameGui.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				GamePanel.this.requestFocus();
			}
		});
	}
	
	public GameManager getGameManager() {
		return gameManager;
	}
	
	public GameBoard getGameBoard() {
		return gameBoard;
	}
	
	public GameGui getGameGui() {
		return gameGui;
	}
	
	public void setGameGui(GameGui gGui) {
		gameGui = gGui;
	}
	
	public void chat(String msg) {
		// parse string and write to chat window
		printOut(froggerClient.getPlayerInfo().getUsername(), msg, true);
		GameChatObject gco = new GameChatObject();
		gco.name = froggerClient.getPlayerInfo().getUsername();
		gco.msg = msg;
		froggerClient.send(gco);
		chatField.setText("");
	}
	
	public void printOut(String name, String msg, boolean self) {
		// make sure multiple threads dont try to print to the textpane at the same time
		// ie ensures if both players send a message at same time, their text won't be spliced together
		chatLock.lock();
		Document d = chatPane.getDocument();
		try {
			if (d.getLength() != 0) {
				d.insertString(d.getLength(), "\n", white);
			}
			if (self) {
				d.insertString(d.getLength(), name + ": ", red);
			} else {
				d.insertString(d.getLength(), name + ": ", blue);
			}
			d.insertString(d.getLength(), msg, black);
			
			chatPane.setCaretPosition(chatPane.getDocument().getLength());
			
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException in ChatPanel");
		} finally {
			chatLock.unlock();
		}
	}
	

	
	//---[ Updating the stats ]---//
	
//	playerStatusPanel.add(yourStatusLabel, BorderLayout.WEST);
//	playerStatusPanel.add(opponentStatusLabel, BorderLayout.EAST);
	
//	Image mystart = Constants.STAGE0UPIMG.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
//	Image theirstart = Constants.STAGE0UPIMG.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
//	
//	yourStatusLabel = new JLabel(new ImageIcon(mystart));
//	opponentStatusLabel = new JLabel(new ImageIcon(theirstart));
	
	public void changeMyPic(ImageIcon ii) {
		Image im = ii.getImage().getScaledInstance(STATUS_SIZE, STATUS_SIZE, Image.SCALE_SMOOTH);
		playerStatusPanel.remove(yourStatusLabel);
		yourStatusLabel = new JLabel(new ImageIcon(im));
		yourStatusLabel.setOpaque(false);
		yourStatusLabel.setBorder(BorderFactory.createEmptyBorder(5,30,5,5));
		playerStatusPanel.add(yourStatusLabel, BorderLayout.WEST);
	}
	
	public void changeTheirPic(ImageIcon ii) {
		Image im = ii.getImage().getScaledInstance(STATUS_SIZE, STATUS_SIZE, Image.SCALE_SMOOTH);
		playerStatusPanel.remove(opponentStatusLabel);
		opponentStatusLabel = new JLabel(new ImageIcon(im));
		opponentStatusLabel.setOpaque(false);
		opponentStatusLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,30));
		playerStatusPanel.add(opponentStatusLabel, BorderLayout.EAST);
	}
	
	public synchronized void updateScore(int score) {
		scoreBox.remove(scoreLabel);
		scoreLabel = new JLabel(""+score);
		scoreLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		scoreLabel.setOpaque(false);
		scoreLabel.setBorder(BorderFactory.createEmptyBorder());
		scoreBox.add(scoreLabel);
		repaint();
		revalidate();
	}
	
	public synchronized void updateHealth(int health){
		healthBox.remove(healthLabel);
		healthLabel = new JLabel();
		if(health == 0) {
			healthLabel.setText("*    ");
		}
		else if(health == 1) {
			healthLabel.setText("**   ");
		}
		else if(health == 2) {
			healthLabel.setText("***  ");
		}
		else if(health == 3) {
			healthLabel.setText("**** ");
			
		} else {
			healthLabel.setText("*****");
		}
		healthLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		healthLabel.setOpaque(false);
		healthLabel.setBorder(BorderFactory.createEmptyBorder());
		healthBox.add(healthLabel);
		repaint();
		revalidate();
	}
	
	public synchronized void updateEnemiesEaten(int numEaten){
		eatenBox.remove(eatenLabel);
		eatenLabel = new JLabel(""+numEaten);
		eatenLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		eatenLabel.setOpaque(false);
		eatenLabel.setBorder(BorderFactory.createEmptyBorder());
		eatenBox.add(eatenLabel);
		repaint();
		revalidate();
	}
	
	public synchronized void updateEvoLevel(int evoLevel){
		evoLevelBox.remove(evoLevelLabel);
		evoLevelLabel = new JLabel(""+evoLevel);
		evoLevelLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		evoLevelLabel.setOpaque(false);
		evoLevelLabel.setBorder(BorderFactory.createEmptyBorder());
		evoLevelBox.add(evoLevelLabel);
		repaint();
		revalidate();
	}
	
	public synchronized void updateTime(long milliseconds){
		int seconds = (int) (milliseconds / 1000) % 60 ;
		int minutes = (int) ((milliseconds / (1000*60)) % 60);
		//int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
		String secs = "" + seconds;
		if (seconds < 10) {
			secs = "0" + seconds;
		}
		String mins = "" + minutes;
		if (minutes < 10) {
			mins = "0" + minutes;
		}
		timeBox.remove(timeLabel);
		timeLabel = new JLabel(/*""+hours+":"+*/mins + ":" + secs);
		timeLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 18));
		timeLabel.setOpaque(false);
		timeLabel.setBorder(BorderFactory.createEmptyBorder());
		timeBox.add(timeLabel);
	}
	
	// --- [win/lose game] --- //
	public void showWinGame(){
		changeMyPic(Constants.MILLERIMG);
		repaint();
		revalidate();
		JDialog jd = new JDialog();
		jd.setSize(500, 300);
		jd.setLocationRelativeTo(null);
		jd.setModal(true);
		JLabel jl = new JLabel(Constants.WIN);
		jd.add(jl);
		jd.setVisible(true);
		//updateTime(0);
	}
	
	public void showLoseGame(){
		changeTheirPic(Constants.MILLERIMG);
		repaint();
		revalidate();
		JDialog jd = new JDialog();
		jd.setSize(500, 300);
		jd.setLocationRelativeTo(null);
		jd.setModal(true);
		JLabel jl = new JLabel(Constants.LOSE);
		jd.add(jl);
		jd.setVisible(true);

		//updateTime(0);
	}
	
	public void printCake(boolean iAteCake) {
		chatLock.lock();
		Document d = chatPane.getDocument();
		try {
			if (d.getLength() != 0) {
				d.insertString(d.getLength(), "\n", white);
			}
			if (iAteCake) {
				d.insertString(d.getLength(), "THE CAKE IS A LIE.", green);
			} else {
				d.insertString(d.getLength(), "THE CAKE WAS A LIE.", green);
			}
			
			chatPane.setCaretPosition(chatPane.getDocument().getLength());
			
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException in ChatPanel");
		} finally {
			chatLock.unlock();
		}
		
	}
	
	public void printFive(boolean iSent5) {
		chatLock.lock();
		Document d = chatPane.getDocument();
		try {
			if (d.getLength() != 0) {
				d.insertString(d.getLength(), "\n", white);
			}
			if (iSent5) {
				d.insertString(d.getLength(), "ONLY FOUR CALORIES", green);
			} else {
				d.insertString(d.getLength(), "NO SUGAR AND ZERO NET CARBS", green);
			}
			
			chatPane.setCaretPosition(chatPane.getDocument().getLength());
			
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException in ChatPanel");
		} finally {
			chatLock.unlock();
		}
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
    	g.drawImage(Constants.LILYPADBG.getImage(), 0, 0, getWidth(), getHeight(), null);
		
	}
}
