package gameclient;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class ClientPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private FroggerClient froggerClient;
	
	// panels and components
		// title screen
	private TitlePanel titlePanel;
	private JButton startButton;
	
		// login screen
	private LoginPanel loginPanel;
	
		// lobby screen
	private LobbyPanel lobbyPanel;
	
		// lobby screen
	private GamePanel gamePanel;
	
	//JMenuItem
	JMenuItem logoutItem, endGameItem;
	
	
	
	public ClientPanel(JMenuItem logoutItem, JMenuItem endGameItem, JMenuItem topScoresItem) {
		super(new CardLayout());
		
		froggerClient = new FroggerClient(this);
		froggerClient.start();
		
		this.logoutItem = logoutItem;
		this.endGameItem = endGameItem;
		topScoresItem.addActionListener(new ActionListener(){
			//override
			public void actionPerformed(ActionEvent ae){
				froggerClient.send("topscores");
			}
		});
		
		
		startButton = new JButton();
		startButton.setIcon(Constants.TITLEBUTTONIMG1);// overwrite paint and depressed icon
		startButton.setPressedIcon(Constants.TITLEBUTTONIMG2);
		startButton.setOpaque(false);
		startButton.setContentAreaFilled(false);
		startButton.setBorder(BorderFactory.createEmptyBorder());
		
		titlePanel = new TitlePanel(startButton);
		add(titlePanel, Constants.TITLEPANEL);
		
		loginPanel = new LoginPanel(froggerClient);
		add(loginPanel, Constants.LOGINPANEL);
		
		lobbyPanel = new LobbyPanel(froggerClient, logoutItem);
		add(lobbyPanel, Constants.LOBBYPANEL);
		
		gamePanel = new GamePanel(froggerClient, endGameItem);
		add(gamePanel, Constants.GAMEPANEL);
		
		
		
		
		// ACTION LISTENERS
		
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				showLogin();
			}
		});
		
	}
	
	//---[ Handling screens ]---//
	public void showStart() {
		CardLayout cl = (CardLayout)this.getLayout();
		cl.show(this, Constants.TITLEPANEL);
		logoutItem.setEnabled(false);
		endGameItem.setEnabled(false);
	}
	
	public void showLogin() {
		CardLayout cl = (CardLayout)this.getLayout();
		cl.show(this, Constants.LOGINPANEL);
		logoutItem.setEnabled(false);
		endGameItem.setEnabled(false);
	}
	
	public void showLobby() {
		CardLayout cl = (CardLayout)this.getLayout();
		cl.show(this, Constants.LOBBYPANEL);
		logoutItem.setEnabled(true);
		endGameItem.setEnabled(false);
	}
	
	public void showGame() {
		CardLayout cl = (CardLayout)this.getLayout();
		cl.show(this, Constants.GAMEPANEL);
		endGameItem.setEnabled(true);
		logoutItem.setEnabled(false);
	}
	
	public void showTopScores(Map<String, Integer> scoresMap){
		JDialog jd = new JDialog();
		jd.setModal(false);
		jd.setSize(400, 500);
		jd.setTitle("Top Scores");
		jd.setLocationRelativeTo(null);
		jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		//create DefaultTableModel
		String [] columnNames = {"Name", "Score"};
		DefaultTableModel scoresModel = new DefaultTableModel(columnNames, 0){
			private static final long serialVersionUID = 234234;
			//override
			public Class getColumnClass(int column){
				switch(column){
				case 0:
					return String.class;
				case 1:
					return Integer.class;
				default:
					return String.class;
				}
			}
		};
		
		//read in username and scores
		for (Map.Entry<String, Integer> entry : scoresMap.entrySet()) {
			scoresModel.addRow(new Object[]{entry.getKey(), entry.getValue()});
		}
		
		//create JTable -> scrollpane -> jd
		JTable scoresTable = new JTable(scoresModel);
		scoresTable.setFont(FontLibrary.getFont("assets/fonts/A Sensible Armadillo.ttf", Font.PLAIN, 24));
		scoresTable.setRowHeight(25);
		jd.add(new JScrollPane(scoresTable));
		
		//sort data
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(scoresModel);
		scoresTable.setRowSorter(sorter);
		List<RowSorter.SortKey> sortKeys = new ArrayList<>();
		int columnIndexToSort = 1;
		sortKeys.add(new RowSorter.SortKey(columnIndexToSort, SortOrder.DESCENDING));
		sorter.setSortKeys(sortKeys);
		sorter.sort();
		
		//alignment
		DefaultTableCellRenderer renderer = 
				(DefaultTableCellRenderer)scoresTable.getDefaultRenderer(Object.class);
		renderer.setHorizontalAlignment(JLabel.LEFT);
		scoresTable.getColumnModel().getColumn(1).setCellRenderer(renderer);
		
		
		jd.setVisible(true);
	}
	
	
	//---[ Getters ]---//
	
	public LoginPanel getLoginPanel() {
		return loginPanel;
	}
	
	public LobbyPanel getLobbyPanel() {
		return lobbyPanel;
	}

	public GamePanel getGamePanel() {
		return gamePanel;
	}
	
	
}
