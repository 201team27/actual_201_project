package gameclient;

import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import communication.PlayerInfo;
import library.FontLibrary;

/* Sets up player profile, using Player object
 * Used in Lobby and player profile window
 */
public class ProfilePanel extends JPanel{
	private static final long serialVersionUID = 945837;
	
	private JLabel usernameLabel, topScoreLabel, numEatenLabel, 
		bestTimeLabel, worstTimeLabel, gamesPlayedLabel, gamesWonLabel;
	public ProfilePanel(PlayerInfo playerInfo){
		
		usernameLabel = new JLabel(playerInfo.getUsername());
		usernameLabel.setOpaque(false);
		usernameLabel.setBorder(BorderFactory.createEmptyBorder());
		topScoreLabel = new JLabel(""+playerInfo.getTopScore());
		topScoreLabel.setOpaque(false);
		topScoreLabel.setBorder(BorderFactory.createEmptyBorder());
		numEatenLabel = new JLabel(""+playerInfo.getNumEaten());
		numEatenLabel.setOpaque(false);
		numEatenLabel.setBorder(BorderFactory.createEmptyBorder());
		bestTimeLabel = new JLabel(""+playerInfo.getBestTime());
		bestTimeLabel.setOpaque(false);
		bestTimeLabel.setBorder(BorderFactory.createEmptyBorder());
		worstTimeLabel = new JLabel(""+playerInfo.getWorstTime()); 
		worstTimeLabel.setOpaque(false);
		worstTimeLabel.setBorder(BorderFactory.createEmptyBorder());
		gamesPlayedLabel = new JLabel(""+playerInfo.getGamesPlayed());
		gamesPlayedLabel.setOpaque(false);
		gamesPlayedLabel.setBorder(BorderFactory.createEmptyBorder());
		gamesWonLabel = new JLabel(""+playerInfo.getGamesWon());
		gamesWonLabel.setOpaque(false);
		gamesWonLabel.setBorder(BorderFactory.createEmptyBorder());
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		//Set up player panel, each label-data pair exists in a panel
		add(new DataPairPanel(new JLabel("User: "), usernameLabel));
		add(new DataPairPanel(new JLabel("Top Score: "), topScoreLabel));
		add(new DataPairPanel(new JLabel("Enemies Eaten: "), numEatenLabel));
		add(new DataPairPanel(new JLabel("Shortest Game: "), bestTimeLabel));
		add(new DataPairPanel(new JLabel("Longest Game: "), worstTimeLabel)); 
		add(new DataPairPanel(new JLabel("Games Played: "), gamesPlayedLabel));
		add(new DataPairPanel(new JLabel("Games Won: "), gamesWonLabel));
		
		setOpaque(false);
		setBorder(BorderFactory.createEmptyBorder());
	}
	
	//updates with new playerInfo
	public void update(PlayerInfo playerInfo){
		usernameLabel.setText(playerInfo.getUsername());
		topScoreLabel.setText(""+playerInfo.getTopScore());
		numEatenLabel.setText(""+playerInfo.getNumEaten());
		bestTimeLabel.setText(""+playerInfo.getBestTime());
		worstTimeLabel.setText(""+playerInfo.getWorstTime()); 
		gamesPlayedLabel.setText(""+playerInfo.getGamesPlayed());
		gamesWonLabel.setText(""+playerInfo.getGamesWon());
		
	}
	
	class DataPairPanel extends JPanel{
		private static final long serialVersionUID = 1L;

		public DataPairPanel(JLabel label, JLabel data){
			label.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 20));
			data.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 20));
			setOpaque(false);
			setBorder(BorderFactory.createEmptyBorder());
			label.setOpaque(false);
			label.setBorder(BorderFactory.createEmptyBorder());
			data.setOpaque(false);
			data.setBorder(BorderFactory.createEmptyBorder());
			
			add(label);
			add(data);
		}
	}

}