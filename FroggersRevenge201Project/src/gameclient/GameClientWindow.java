package gameclient;


import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import library.FontLibrary;

/*
 * The main window for Sorry!
 * */
public class GameClientWindow extends JFrame {
	
	private static final long serialVersionUID = 5147395078473323173L;
	
	//Dimensions for the game
	//private final static Dimension minSize = new Dimension(640,480);
	private final static Dimension maxSize = new Dimension(980,660);
	
	// MenuBar
	private JMenuBar jmb;
	JMenuItem websiteItem;
	JMenuItem topScoresItem;
	JMenuItem rulesItem;
	JMenuItem endGameItem;
	JMenuItem logoutItem;
	JMenuItem exitItem;
	
	// Menu Dialog Boxes
	private JDialog topScoresDialog; 
	private JDialog rulesDialog; 
	
	// Game Layouts
	private ClientPanel clientPanel;
	
	// Buttons
		// Start Button
	
	public GameClientWindow() { //Construct
		// UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("A3_Assets/fonts/kenvector_future.ttf", Font.PLAIN, 14));
		// setUIFont (new javax.swing.plaf.FontUIResource(new Font("MS Mincho",Font.PLAIN, 12)));
		//setLayout(new CardLayout());
		setTitle("Frogger's Revenge!!! 'Allo Jofrey");
		setSize(maxSize);
		setMinimumSize(maxSize);
		setMaximumSize(maxSize);
		
		// UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("assets/fonts/attack of the cucumbers.ttf", Font.PLAIN, 18));
		
		// token change
		jmb = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		
		websiteItem = new JMenuItem("Website");
		topScoresItem = new JMenuItem("Top Scores");
		rulesItem = new JMenuItem("Rules");
		endGameItem = new JMenuItem("End Game");
		logoutItem = new JMenuItem("Logout");
		exitItem = new JMenuItem("Exit");
		
		menu.add(websiteItem);
		menu.add(topScoresItem);
		menu.add(rulesItem);
		menu.add(endGameItem);
		menu.add(logoutItem);
		menu.add(exitItem);
		
		setMenuActionListeners();
		
		jmb.add(menu);
		
		setJMenuBar(jmb);
		
		endGameItem.setEnabled(false);
		logoutItem.setEnabled(false);
		
		clientPanel = new ClientPanel(logoutItem, endGameItem, topScoresItem);
		add(clientPanel);
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		Toolkit toolKit = Toolkit.getDefaultToolkit();
		Image cursorImage = toolKit.getImage("assets/images/cursorHand_grey.png");
		Cursor custom = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(0,0),"");
		setCursor(custom);
		
		logoutItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				clientPanel.showLogin();
			}
		});
	}

	
	// Menu Bar Action Listeners 
	private void setMenuActionListeners() {
		websiteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    Desktop.getDesktop().browse(new URI("http://www-scf.usc.edu/~renachen/FroggersRevenge/?nomo=1&hl=fr"));
                } catch (URISyntaxException | IOException ex) {
                    //It looks like there's a problem
                }
            }
        });
		rulesItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				rulesDialog = new JDialog(); 
				rulesDialog.setLocation(300,100);
				rulesDialog.setSize(500,400);
				rulesDialog.setTitle("Rules");
				JPanel panel = new JPanel(); 
				JTextArea jta=new JTextArea("GAME RULES:"+"\n"+"\n"+"- The player will start out being represented as a tadpole (the lowest level of evolution)."
				+"\n"+"- After a player gains enough points at every level, they will evolve to the next (higher) level of evolution."
				+"\n"+"- The player can only move up, down, left, right."
				+"\n"+"- The player can only move on lilypads and must always stay on the board."
				+"\n"+"- Enemies will randomly spawn on the edges of the board, then move across the board in one direction."
				+"\n"+"- If an enemy runs into a player and is at a higher level than the player, it will harm the player then keep moving. Else the player will eat the enemy."
				+"\n"+"- Items (cake, candy and five hour energy) will randomly spawn on lilypads to be eaten by the player. Each item has a different effect on the player and/or their opponent on a separate board."
				+"\n"+"- A game is done once one player reaches the final evolution (the Miller). That player will be declared the winner and the game will end for both players."
				+"\n"+"- If one player rage quits before the game ends, the other player will automatically win."+"\n"
				+"\n"+"EVOLUTION STATES:"+"\n"+"\n"+"- Tadpole"+"\n"+"- Half tadpole"+"\n"+"- Adult frog"+"\n"+"- Rainbow Frog"+"\n"+"- Miller Frog"+"\n"); 
				jta.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 20));
				jta.setLineWrap(true);
				jta.setWrapStyleWord(true);
				jta.setEditable(false);
				JScrollPane jsp=new JScrollPane(jta);
				rulesDialog.add(jsp);
				//rulesDialog.add(panel);
				rulesDialog.setVisible(true);
			}
		});
		
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JOptionPane frame = new JOptionPane(); 
				int response = JOptionPane.showConfirmDialog(
						frame,
						"Are you sure you want to exit?",
						"Confirm Exit",
						JOptionPane.YES_NO_OPTION); 
				if (response == JOptionPane.YES_OPTION){ 
					
					// *** Need to add end game/log out functionality here
					dispatchEvent(new WindowEvent(GameClientWindow.this, WindowEvent.WINDOW_CLOSING));
				}
			}
		});
		
		
	}
	

	
	
	public static void main(String[] args) {
		//Create a new SorryClient Window
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	new GameClientWindow();
		    }
		});
	}
}

// stuff from .gitignore
///FroggersRevenge201Project/.project
///FroggersRevenge201Project/.classpath
///FroggersRevenge201Project/.settings/
