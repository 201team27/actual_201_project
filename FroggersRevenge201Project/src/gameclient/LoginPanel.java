package gameclient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import communication.LoginObject;
import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class LoginPanel extends JPanel {
	
	private static final long serialVersionUID = 4210249007664388072L;
	
	private JTextField nameField;
	private JPasswordField passwordField;
	private JButton loginButton;
	private JButton registerButton;
	private JButton guestStartButton;
	private FroggerClient froggerClient;
	
	public LoginPanel(FroggerClient fClient) {
		super(new GridBagLayout());
		
		froggerClient = fClient;
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		// LEFT SIDE OF SCREEN
		
		// panel for left
		JPanel leftPanel = new JPanel(new GridBagLayout());
		leftPanel.setOpaque(false);
		//leftPanel.setContentAreaFilled(false);
		leftPanel.setBorder(BorderFactory.createEmptyBorder());
		
		// label for left heading (please login)
		JLabel pleaseLoginFrog = new JLabel(Constants.PLEASELOGINIMG);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		add(pleaseLoginFrog, gbc);
		gbc.gridwidth = 1;
		
		JPanel loginLayout = new JPanel(new FlowLayout());
		loginLayout.setOpaque(false);
		loginLayout.setBorder(BorderFactory.createEmptyBorder());
		
		JLabel loginPassLabel = new JLabel(Constants.LOGINPASSIMG);
		loginLayout.add(loginPassLabel);
		
		// houses the name text field and password text field
		JPanel textfieldPanel = new JPanel(new GridBagLayout());
		textfieldPanel.setOpaque(false);
		textfieldPanel.setBorder(new EmptyBorder(new Insets(0,0,0,0)));
		
		nameField = new JTextField(20);
		nameField.setMargin(new Insets(2,2,2,2));
		nameField.setFont(FontLibrary.getFont("assets/fonts/A Sensible Armadillo.ttf", Font.BOLD, 24));
		////////////
		// used to create space between textfields in 'textfieldPanel'
		JLabel textfieldGap = new JLabel("   ");
		
		passwordField = new JPasswordField(20);
		passwordField.setMargin(new Insets(8,2,4,2));
		passwordField.setFont(new Font("serif", Font.BOLD, 19));
				
		textfieldPanel.add(nameField, gbc);
		
		gbc.gridy = 1;
		textfieldPanel.add(textfieldGap, gbc);
		
		gbc.gridy = 2;
		textfieldPanel.add(passwordField, gbc);
		
		loginLayout.add(textfieldPanel);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		leftPanel.add(loginLayout, gbc);
		
		loginButton = new JButton();
		Image loginImg1 = Constants.LOGINBUTTONIMG1.getImage().getScaledInstance(260, 80, Image.SCALE_SMOOTH);
		loginButton.setIcon(new ImageIcon(loginImg1));// overwrite paint and depressed icon
		Image loginImg2 = Constants.LOGINBUTTONIMG2.getImage().getScaledInstance(260, 80, Image.SCALE_SMOOTH);
		loginButton.setPressedIcon(new ImageIcon(loginImg2));
		loginButton.setOpaque(false);
		loginButton.setContentAreaFilled(false);
		loginButton.setBorder(BorderFactory.createEmptyBorder());
		
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				LoginObject lo = new LoginObject();
				lo.name = nameField.getText();
				char[] chars = passwordField.getPassword();
				lo.pass = new String(chars);
				System.out.println("Trying to log in from LoginPanel");
				froggerClient.send(lo);
			}
		});
		
		gbc.gridy = 2;
		leftPanel.add(loginButton, gbc);
		
		Image noAccountImg = Constants.DONTHAVEACCOUNTIMG.getImage().getScaledInstance(440, 120, Image.SCALE_SMOOTH);
		JLabel dontHaveAccount = new JLabel(new ImageIcon(noAccountImg));
		gbc.gridy = 3;
		leftPanel.add(dontHaveAccount, gbc);
		
		registerButton = new JButton();
		Image registerImg1 = Constants.REGISTERBUTTONIMG1.getImage().getScaledInstance(280, 100, Image.SCALE_SMOOTH);
		registerButton.setIcon(new ImageIcon(registerImg1));
		Image registerImg2 = Constants.REGISTERBUTTONIMG2.getImage().getScaledInstance(280, 100, Image.SCALE_SMOOTH);
		registerButton.setPressedIcon(new ImageIcon(registerImg2));
		registerButton.setOpaque(false);
		registerButton.setContentAreaFilled(false);
		registerButton.setBorder(BorderFactory.createEmptyBorder());
		
		// register button functionality
		registerButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				JDialog jd = new JDialog();
				RegistrationPanel rp = new RegistrationPanel(froggerClient, jd);
				jd.setTitle("Register New Account");
				jd.setMinimumSize(new Dimension(500,300));
				jd.add(rp);
				jd.setModal(true);
				jd.setLocationRelativeTo(null);
				
				jd.setVisible(true);
				
				
			}
		}); 
		
		gbc.gridy = 4;
		leftPanel.add(registerButton, gbc);
		
		
		// add left side graphics to the overall layout
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(leftPanel, gbc);
		
		
		// CENTER LINE
		// needs picture from Christina
		JLabel lineLabel = new JLabel(Constants.DIVIDERIMG);
		
		gbc.gridx = 1;
		add(lineLabel, gbc);
		
		
		// RIGHT SIDE OF SCREEN
		
		// panel for right
		JPanel rightPanel = new JPanel(new GridBagLayout());
		rightPanel.setOpaque(false);
		//leftPanel.setContentAreaFilled(false);
		rightPanel.setBorder(BorderFactory.createEmptyBorder());
		gbc.gridx = 0;
		gbc.gridy = 0;
		JLabel playAsGuestLabel = new JLabel(Constants.PLAYASGUESTIMG);
		
		rightPanel.add(playAsGuestLabel, gbc);
		
		guestStartButton = new JButton();
		Image guestStartImg1 = Constants.GUESTSTARTBUTTONIMG1.getImage().getScaledInstance(280, 100, Image.SCALE_SMOOTH);
		guestStartButton.setIcon(new ImageIcon(guestStartImg1));// overwrite paint and depressed icon
		Image guestStartImg2 = Constants.GUESTSTARTBUTTONIMG2.getImage().getScaledInstance(280, 100, Image.SCALE_SMOOTH);
		guestStartButton.setPressedIcon(new ImageIcon(guestStartImg2));
		guestStartButton.setOpaque(false);
		guestStartButton.setContentAreaFilled(false);
		guestStartButton.setBorder(BorderFactory.createEmptyBorder());
		guestStartButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
//				System.out.println("Guest login");
//				froggerClient.send("hackintolobby");
				froggerClient.send("gueststart");
				
				
			}
		});
		gbc.gridy = 1;
		rightPanel.add(guestStartButton, gbc);
		
		gbc.gridx = 2;
		gbc.gridy = 1;
		add(rightPanel, gbc);
	}
	
	public void clearFields() {
		nameField.setText("");;
		passwordField.setText("");
	}
	
	public void userAlreadyIn(){
		JLabel taken = new JLabel(Constants.ALREADYINIMG);
		JDialog jd = new JDialog();
		jd.setMinimumSize(new Dimension(500,300));
		jd.add(taken);
		jd.setModal(true);
		jd.setLocationRelativeTo(null);
		jd.setVisible(true);
	}
	
	public void userDoesNotExist(){
		JLabel taken = new JLabel(Constants.NOTEXISTIMG);
		JDialog jd = new JDialog();
		jd.setMinimumSize(new Dimension(500,300));
		jd.add(taken);
		jd.setModal(true);
		jd.setLocationRelativeTo(null);
		jd.setVisible(true);
	}
	
	public void loginUnsuccessful() {
		JLabel taken = new JLabel(Constants.NOTMATCHIMG);
		JDialog jd = new JDialog();
		jd.setMinimumSize(new Dimension(500,300));
		jd.add(taken);
		jd.setModal(true);
		jd.setLocationRelativeTo(null);
		jd.setVisible(true);
	}
	
	public void registerUnsuccessful() {
		JLabel taken = new JLabel(Constants.TAKENIMG);
		JDialog jd = new JDialog();
		jd.setMinimumSize(new Dimension(500,300));
		jd.add(taken);
		jd.setModal(true);
		jd.setLocationRelativeTo(null);
		jd.setVisible(true);
		registerButton.doClick();
	}
	
	public void registerSuccessful() {
		JLabel taken = new JLabel(Constants.SUCCESSIMG);
		JDialog jd = new JDialog();
		jd.setMinimumSize(new Dimension(500,300));
		jd.add(taken);
		jd.setModal(true);
		jd.setLocationRelativeTo(null);
		jd.setVisible(true);
	}
	
	public void wrongLogin(){
		clearFields();
		JDialog jd = new JDialog();
		jd.setMinimumSize(new Dimension(500,300));
		jd.setLocationRelativeTo(null);;
		jd.add(new JLabel(Constants.INVALIDIMG));
		jd.setModal(true);
		jd.setVisible(true);
	}
	
	//@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(new Color(255,255,255));
			g.fillRect(0, 0, getWidth(), getHeight());
			// Graphics2D g2d = (Graphics2D) g.create();
			
			g.drawImage(Constants.CLOUDIMG.getImage(), 0, 0, getWidth(), getHeight(), null);
		}

}
