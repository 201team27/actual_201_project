package gameclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import communication.RegisterObject;
import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class RegistrationPanel extends JPanel {

	
	private static final long serialVersionUID = 7995490291137272364L;
	
	private JTextField nameField;
	private JPasswordField passwordField;
	private JButton createButton;
	private FroggerClient froggerClient;
	private JDialog mparentJD;
	
	public RegistrationPanel(FroggerClient fClient, JDialog parentJD) {
		super();
		froggerClient = fClient;
		mparentJD = parentJD;
		setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		
		JLabel loginPassLabel = new JLabel(Constants.LOGINPASSIMG);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(loginPassLabel, gbc);
		
		// Text Fields
		
		JPanel textfieldPanel = new JPanel(new GridBagLayout());
		textfieldPanel.setOpaque(false);
		textfieldPanel.setBorder(new EmptyBorder(new Insets(0,0,0,0)));
		
		nameField = new JTextField(18);
		nameField.setMargin(new Insets(2,2,2,2));
		nameField.setFont(FontLibrary.getFont("assets/fonts/A Sensible Armadillo.ttf", Font.BOLD, 20));

		// used to create space between textfields in 'textfieldPanel'
		JLabel textfieldGap = new JLabel("   ");
		
		passwordField = new JPasswordField(18);
		passwordField.setMargin(new Insets(20,2,8,2));
		//passwordField.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 24));
		passwordField.setFont(new Font("serif", Font.BOLD, 15));
		gbc.gridy = 0;
		textfieldPanel.add(nameField, gbc);
		
		gbc.gridy = 1;
		textfieldPanel.add(textfieldGap, gbc);
		
		gbc.gridy = 2;
		textfieldPanel.add(passwordField, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(textfieldPanel, gbc);
		
		createButton = new JButton();
		createButton.setIcon(Constants.CREATEBUTTONIMG1);// overwrite paint and depressed icon
		createButton.setPressedIcon(Constants.CREATEBUTTONIMG2);
		createButton.setOpaque(false);
		createButton.setContentAreaFilled(false);
		createButton.setBorder(BorderFactory.createEmptyBorder());
		
		createButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				String name = nameField.getText();
				char [] chars = passwordField.getPassword();
				String pass = new String(chars);
				JLabel message = null;
				boolean badName = false;
				boolean badPass = false;
				
				if (name.length() > 2) {
					if (name.startsWith("guest")) {
						message = new JLabel(Constants.NOSTARTGUESTIMG);
						badName = true;
					}
					
					for (int i = 0; i < name.length(); i++) {
						if (!isLetterOrDigit(name.charAt(i))) {
							// Will replace with images
							message = new JLabel(Constants.USERNAMECANIMG);
							badName = true;
						}
					}
				} else {
					message = new JLabel(Constants.THREECHARMESSAGEIMG);
					badName = true;
				}
				
				if (badName) {
					
					JDialog jd = new JDialog();
					JPanel jp = new JPanel(new BorderLayout());
					jp.add(message);
					jd.add(jp);
					jd.setTitle("");
					jd.setMinimumSize(new Dimension(500,300));
					jd.setModal(true);
					jd.setLocationRelativeTo(null);
					jd.setVisible(true);
										
					nameField.setText("");
					passwordField.setText("");
					
				// name is valid, check the password
				} else {
					if (pass.length() < 6) {
						// will be replaced with vector art
						message = new JLabel(Constants.SIXCHARPASSIMG);
						badPass = true;
					} else {
						for (int i = 0; i < pass.length(); i++) {
							if (pass.charAt(i) == ' ' || pass.charAt(i) == '\t' || pass.charAt(i) == '\n') {
								message = new JLabel(Constants.WHITESPACEIMG);
								badPass = true;
							}
						}
					}
					
					if (badPass) {
						
						JDialog jd = new JDialog();
						JPanel jp = new JPanel(new BorderLayout());
						jp.add(message);
						jd.add(jp);
						jd.setTitle("");
						jd.setMinimumSize(new Dimension(500,300));
						jd.setModal(true);
						jd.setLocationRelativeTo(null);
						jd.setVisible(true);
						
						passwordField.setText("");
						
					} else {
						RegisterObject ro = new RegisterObject();
						ro.name = name;
						ro.pass = pass;
						froggerClient.send(ro);
						
						mparentJD.dispose();
					}
				}
			}
		}); 
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 2;
		JLabel buttonBuffer1 = new JLabel("   ");
		add(buttonBuffer1, gbc);
		
		gbc.gridy = 3;
		JLabel buttonBuffer2 = new JLabel("   ");
		add(buttonBuffer2, gbc);
		
		gbc.gridy = 4;
		add(createButton, gbc);
	}
	
	 //taken from http://stackoverflow.com/questions/4047808/what-is-the-best-way-to-tell-if-a-character-is-a-letter-or-number-in-java-withou
	private boolean isLetterOrDigit(char c) {
	    return (c >= 'a' && c <= 'z') ||
	           (c >= 'A' && c <= 'Z') ||
	           (c >= '0' && c <= '9');
	}
	
	//@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLUE/*new Color(255,255,255)*/);
		g.fillRect(0, 0, getWidth(), getHeight());
		// Graphics2D g2d = (Graphics2D) g.create();
		
		g.drawImage(Constants.FLUFFYCLOUDIMG.getImage(), 0, 0, getWidth(), getHeight(), null);
	}

}
