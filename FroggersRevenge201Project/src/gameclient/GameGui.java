//GAMEGUI
package gameclient; 
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import game.GuiEnemy;
import library.Constants;

public class GameGui extends JPanel{
	public static final long serialVersionUID = 1; 
	private LilyPadGui[][] tileTracker; 
	private int xpos; 
	private int ypos; 
	private int oldx; 
	private int oldy; 
	private int currState; 
	private String dir;
	private int type;
	private Vector<GuiEnemy> enemyVector;
	public Lock enemyLock;
	//type 0: default frog
	//type 1: redXfrog
	//type 2: immunefrog
	//type 3: godfrog
	private ImageIcon bgImage;
	
	public GameGui(){
		enemyVector=new Vector<GuiEnemy>();
		enemyLock=new ReentrantLock();
		tileTracker = new LilyPadGui[8][8];
		createGui(); 
		addEvents(); 
		currState = 0; 
		xpos = 4; 
		ypos = 4;
		oldx = 4; 
		oldy = 4; 
		type = 0; 
		setFocusable(true); 
		setFocusTraversalKeysEnabled(false); 
		setOpaque(false);
		bgImage = Constants.WATERIMG; 
	}
	
	public void resetGui(){
		currState = 0; 
		xpos = 4; 
		ypos = 4;
		oldx = 4; 
		oldy = 4; 
		type = 0;
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				tileTracker[j][i].changeDefault(); 
			}
		}
		tileTracker[4][4].change(0,"right",type);
		
		tileTracker[1][1].putItem("blank");
		tileTracker[2][1].putItem("blank");
		tileTracker[6][0].putItem("blank");
		tileTracker[4][2].putItem("blank");
		tileTracker[7][2].putItem("blank");
		tileTracker[0][3].putItem("blank");
		tileTracker[2][4].putItem("blank");
		tileTracker[3][4].putItem("blank");
		tileTracker[5][4].putItem("blank");
		tileTracker[1][5].putItem("blank");
		tileTracker[5][5].putItem("blank");
		tileTracker[7][5].putItem("blank");
		tileTracker[3][6].putItem("blank");
		tileTracker[5][6].putItem("blank");
	}
	
	
	private void createGui(){
		setSize(700,700);
		setLayout(new GridLayout(8,8)); 
		setBorder(BorderFactory.createLineBorder(Color.black));
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				LilyPadGui hahaha = new LilyPadGui(); 
				hahaha.setOpaque(false);
				add(hahaha);
				tileTracker[j][i] = hahaha; 
			}
		}
		tileTracker[4][4].change(0,"right",type);
		
		tileTracker[1][1].putItem("blank");
		tileTracker[2][1].putItem("blank");
		tileTracker[6][0].putItem("blank");
		tileTracker[4][2].putItem("blank");
		tileTracker[7][2].putItem("blank");
		tileTracker[0][3].putItem("blank");
		tileTracker[2][4].putItem("blank");
		tileTracker[3][4].putItem("blank");
		tileTracker[5][4].putItem("blank");
		tileTracker[1][5].putItem("blank");
		tileTracker[5][5].putItem("blank");
		tileTracker[7][5].putItem("blank");
		tileTracker[3][6].putItem("blank");
		tileTracker[5][6].putItem("blank");
	}
	

	
	
	private void addEvents(){
	}
	
	//Player movement shizz
	public void moveRight(){
		dir = "right";
        if(xpos != 7){
        	oldx = xpos; 
        	oldy = ypos;
        	xpos = xpos + 1; 
        }
        updateBoard();
	}
	public void moveDown(){
		dir = "down";
        if(ypos != 7){
        	oldx = xpos;
        	oldy = ypos; 
        	ypos = ypos + 1;
        }
        updateBoard();
	}
	public void moveLeft(){
		dir = "left";
        if(xpos != 0){
        	oldx = xpos;
        	oldy = ypos;
        	xpos = xpos - 1; 
        }
        updateBoard();
	}
	public void moveUp(){
		dir = "up"; 
        if(ypos != 0){
        	oldx = xpos;
        	oldy = ypos; 
        	ypos = ypos -1; 
        }
        updateBoard();
	}
	
	public void updateBoard(){
		tileTracker[oldx][oldy].changeDefault();
		tileTracker[xpos][ypos].change(currState,dir,type);
	}
	
	public void putItem(int x, int y, String item){
		tileTracker[x][y].putItem(item);
	}
	
	public void setGod(){
		type = 3;
	}
	
	public void setImmune(){
		type = 2; 
	}
	
	public void setRed(){
		type = 1; 
	}
	
	public void setNormal(){
		type = 0; 
	}
	
	public int getType(){
		return type; 
	}
	
	public Vector<GuiEnemy> getEnemies(){
		return enemyVector; 
	}
	
	public void setCurrState(int level){
		currState = level; 
	}
	
//	@Override
//	public void paintComponent(Graphics g){
//		
//		g.drawImage(bgImage.getImage(), 0, 0,getWidth(),getHeight(),this);
//		//super.paintComponent(g);
//		enemyLock.lock();
//		
//		for(GuiEnemy ge:enemyVector){
//			g.drawImage(ge.getImage(),ge.getX(),ge.getY(),getWidth()/8,getHeight()/8,null);
//		}
//		enemyLock.unlock();
//		
//	}
	
	@Override
	public void paint(Graphics g) {
		
//		g.drawImage(bgImage.getImage(), 0, 0,getWidth(),getHeight(),this);
		super.paint(g);
		
		g.drawImage(bgImage.getImage(), 0, 0,getWidth(),getHeight(),this);
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				tileTracker[j][i].paint(g, this, j*getWidth()/8, i*getHeight()/8);
			}
		}
		enemyLock.lock();
		
		for(GuiEnemy ge:enemyVector){
			g.drawImage(ge.getImage(),ge.getX(),ge.getY(),getWidth()/8,getHeight()/8,null);
		}
		enemyLock.unlock();
		
//		Graphics2D g2d = (Graphics2D) g;
//		g2d.setColor(Color.RED);
//		g2d.fillOval(0, 0, 30, 30);
//		g2d.drawOval(0, 50, 30, 30);		
//		g2d.fillRect(50, 0, 30, 30);
//		g2d.drawRect(50, 50, 30, 30);
//
//		g2d.draw(new Ellipse2D.Double(0, 100, 30, 30));
	}
	
}