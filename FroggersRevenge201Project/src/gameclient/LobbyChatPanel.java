package gameclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import communication.LobbyChatObject;
import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class LobbyChatPanel extends JPanel {

	private static final long serialVersionUID = -8830753598607800845L;
	
	private FroggerClient froggerClient;
	
	private JTextPane chatPane;
	private JTextArea chatField;
	private JButton chatButton;
	private JScrollPane sp;
	private SimpleAttributeSet black;
	private Color myColor;
	
	public LobbyChatPanel(FroggerClient froggerClient) {
		super();

		this.froggerClient = froggerClient;
		setLayout(new BorderLayout());
		
		Random rand = new Random();
		
		float col1 = rand.nextFloat()/2 + (float).3;
		float col2 = rand.nextFloat()/2 + (float).3;
		float col3 = rand.nextFloat()/2 + (float).3;
		
		myColor = new Color(col1, col2, col3);
		
		JLabel chatHeading = new JLabel("CHAT:");
		chatHeading.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		add(chatHeading, BorderLayout.NORTH);
		
		chatPane = new JTextPane();
		chatPane.setEditable(false);
		
		//chatPane.setBackground(Color.BLACK);
		//chatPane.setForeground(Color.WHITE);
		chatPane.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 24));
		
		black = new SimpleAttributeSet();
		StyleConstants.setForeground(black, Color.BLACK);
		
		
		this.setPreferredSize(new Dimension(100,100));
		
		sp = new JScrollPane(chatPane);
		//sp.setBounds(10,60,780,500);
		sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		sp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		add(sp);
		
		JPanel textEntryPanel = new JPanel(new BorderLayout());
		chatField = new JTextArea();
		//chatField.setFont(Constants.GAMEFONT);
//		chatField.setBackground(Color.WHITE);
//		chatField.setForeground(Color.BLACK);
		chatField.setLineWrap(true);
		chatField.setWrapStyleWord(true);
		chatField.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 24));
		JScrollPane spField = new JScrollPane(chatField);
		spField.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		spField.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		//chatField.setFont(FontLibrary.getFont("fonts/kenvector_future_thin.ttf", Font.PLAIN, 14));
		
		textEntryPanel.add(spField);
		
		chatButton = new JButton();
		//chatButton.setPreferredSize(new Dimension(100,100));
		Image chatImg1 = Constants.SENDBUTTONIMG1.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		chatButton.setIcon(new ImageIcon(chatImg1));// overwrite paint and depressed icon
		Image chatImg2 = Constants.SENDBUTTONIMG2.getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
		chatButton.setPressedIcon(new ImageIcon(chatImg2));
		chatButton.setOpaque(false);
		chatButton.setContentAreaFilled(false);
		chatButton.setBorder(BorderFactory.createEmptyBorder());
		
		chatButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!chatField.getText().equals("")) {
					chat(chatField.getText());
				}
				//chatArea.append("\n" + chatField.getText());
			}
		});
		
		chatField.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent ke) {
				if(ke.getKeyCode() == KeyEvent.VK_ENTER){
					ke.consume();
					if (!chatField.getText().equals("")) {
						chat(chatField.getText());
					}
	            }
			}
		});
		
		textEntryPanel.add(chatButton, BorderLayout.EAST);
		textEntryPanel.setOpaque(false);	
		add(textEntryPanel, BorderLayout.SOUTH);
		
	}
	
	public void chat(String msg) {
		// parse string and write to chat window
		LobbyChatObject lco = new LobbyChatObject();
		lco.name = froggerClient.getPlayerInfo().getUsername();
		lco.msg = msg;
		lco.col = myColor;
		froggerClient.send(lco);
		
		// fix this
		printOut(lco.name, msg, myColor);
		chatField.setText("");
	}
	
	public synchronized void printOut(String name, String msg, Color col) {
		Document d = chatPane.getDocument();
		try {
			SimpleAttributeSet color = new SimpleAttributeSet();
			StyleConstants.setForeground(color, col);
			
			if (d.getLength() != 0) {
				d.insertString(d.getLength(), "\n", color);
			}
			
			d.insertString(d.getLength(), name + ": ", color);
			d.insertString(d.getLength(), msg, black);
			
			chatPane.setCaretPosition(chatPane.getDocument().getLength());
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException in LobbyChatPanel");
		}
	}
	
	public synchronized void clearText() {
		chatPane.setText("");
		chatField.setText("");
	}
	
	public void disableChatButton() {
		chatButton.setEnabled(false);
	}
	
	public void enableChatButton() {
		chatButton.setEnabled(true);
	}

}
