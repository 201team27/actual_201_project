package gameclient;

import java.awt.Dimension;
import java.util.Random;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JLabel;

import library.Constants;

public class AdvertisementThread extends Thread {
	
	private Vector<JLabel> adLabels;
	private Random rand;
	
	public AdvertisementThread() {
		
		//adLabels = new Vector<JLabel>();
		//adLabels.add(new JLabel("add 1 image"));
		//adLabels.add(new JLabel("add 2 image"));
		//adLabels.add(new JLabel("add 3 image"));
		//adLabels.add(new JLabel("add 4 image"));
		//adLabels.add(new JLabel("add 5 image"));
		
		adLabels = new Vector<JLabel>();
		adLabels.add(new JLabel(Constants.AD1));
		adLabels.add(new JLabel(Constants.AD2));
		adLabels.add(new JLabel(Constants.AD3));
		adLabels.add(new JLabel(Constants.AD4));
		adLabels.add(new JLabel(Constants.AD5));
		adLabels.add(new JLabel(Constants.AD6));
		
		rand = new Random();
		
		start();
	}
	
	public void run() {
		try {
			int choice = 0;
			while(true) {
		        Thread.sleep(45000);
		        
		        JDialog jd = new JDialog();
		        jd.add(adLabels.elementAt(choice));
		        jd.setMinimumSize(new Dimension(300, 230));
		        jd.setLocationRelativeTo(null);
		        jd.setVisible(true);
		        
		        choice += 1;
		        choice = choice%adLabels.size();
			}
			
	    } catch (InterruptedException ie) {
	    	System.out.println("Advertisements Ceased");
	        return;
	    }
	}
}
