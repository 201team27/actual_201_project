package gameclient;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import communication.ChallengeObject;
import communication.EnterLobbyObject;
import communication.PlayerInfo;
import communication.ViewProfileObject;
import game.GameManager;
import game.GuiEnemy;
import library.Constants;
import library.FontLibrary;
import networking.FroggerClient;

public class LobbyPanel extends JPanel {

	private static final long serialVersionUID = 3439434;
	
	//Components
	private JButton challengeButton, viewProfileButton;
	private DefaultListModel<String> playerSelectModel;
	private JList<String> playerSelectList;
	private LobbyChatPanel lobbyChatPanel;
	
	private FroggerClient froggerClient;
	private ProfilePanel playerProfile;
	
	public LobbyPanel(FroggerClient fClient, JMenuItem logoutItem){
		setLayout(new BorderLayout(10, 10));
		setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
		froggerClient = fClient;
		
		createGUI();
		addEvents();
		
		logoutItem.addActionListener(new ActionListener(){
			//override
			public void actionPerformed(ActionEvent ae){
				EnterLobbyObject elo = new EnterLobbyObject();
				elo.entering = false;
				elo.name = froggerClient.getPlayerInfo().getUsername();
				froggerClient.send(elo);
				lobbyChatPanel.clearText();
				clearPlayerList();
				froggerClient.logout();
			}
		});
	}
	
	//---[ Accessors and Modifiers for Player List]---
	
	public void addPlayer(String name){
		playerSelectModel.addElement(name);
	}
	
	public void removePlayer(String name){
		playerSelectModel.removeElement(name);
	}
	
	public void removePlayerAt(int index){
		playerSelectModel.remove(index);
	}
	
	public void clearPlayerList(){
		playerSelectModel.removeAllElements();
	}
	
	
	//---[ Setup of GUI and ActionListeners]--
	
	private void createGUI(){

		// Instantiate playerProfile with a placeholder and then set playerInfo later 
		PlayerInfo placeHolder = new PlayerInfo("testPlayer",0,0,"00:00:00","00:00:00",0,0);
		playerProfile = new ProfilePanel(placeHolder);
		playerProfile.setOpaque(false);
		playerProfile.setBorder(BorderFactory.createEmptyBorder());
		
		
		// chat (center)
		lobbyChatPanel = new LobbyChatPanel(froggerClient);
		lobbyChatPanel.setOpaque(false);
		lobbyChatPanel.setBorder(BorderFactory.createEmptyBorder());
		
		
		// player selection (right)
		challengeButton = new JButton();
		viewProfileButton = new JButton();
		playerSelectModel = new DefaultListModel<String>();
		playerSelectList = new JList<String>(playerSelectModel);
		
		playerSelectList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
		
		challengeButton.setIcon(Constants.CHALLENGEBUTTONIMG1);
		challengeButton.setPressedIcon(Constants.CHALLENGEBUTTONIMG2);
		challengeButton.setBorder(BorderFactory.createEmptyBorder());
		challengeButton.setContentAreaFilled(false);
		viewProfileButton.setIcon(Constants.VIEWPROFILEIMG1);
		viewProfileButton.setPressedIcon(Constants.VIEWPROFILEIMG2);
		viewProfileButton.setBorder(BorderFactory.createEmptyBorder());
		viewProfileButton.setContentAreaFilled(false);
		
		JScrollPane scrollPane = new JScrollPane(playerSelectList);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JLabel playersLabel = new JLabel("Available Players:");
		playersLabel.setFont(FontLibrary.getFont("assets/fonts/schoolbully.ttf", Font.BOLD, 30));
		playersLabel.setOpaque(false);
		playersLabel.setBorder(BorderFactory.createEmptyBorder());
		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		right.add(playersLabel);
		right.add(scrollPane);
		right.add(Box.createVerticalStrut(10));
		right.add(challengeButton);
		right.add(Box.createVerticalStrut(10));
		right.add(viewProfileButton);
		right.add(Box.createVerticalStrut(10));
		right.setPreferredSize(new Dimension(280, 100));
		right.setOpaque(false);
		right.setBorder(BorderFactory.createEmptyBorder());
		
		
		//add components to overall GUI
		add(playerProfile, BorderLayout.WEST);
		add(lobbyChatPanel, BorderLayout.CENTER);
		add(right, BorderLayout.EAST);
	}
	
	private void addEvents(){
		challengeButton.addActionListener(new ActionListener(){
			//override
			public void actionPerformed(ActionEvent ae){
				int index = playerSelectList.getSelectedIndex();
				//send to server that we want to challenge this user (if someone is selected and this is not a guest
				if (index > -1 && !froggerClient.getPlayerInfo().getUsername().startsWith("guest")) {
					// this needs to be deleted
					// froggerClient.showGame();
					String challenged = playerSelectList.getSelectedValue();//.getComponent(index);
					
					// if you didn't challenge yourself
					if (!challenged.equals(froggerClient.getPlayerInfo().getUsername())) {
						// make challenge object
						ChallengeObject co = new ChallengeObject();
						co.challenger = froggerClient.getPlayerInfo().getUsername();
						co.challenged = challenged;
						disableLobbyFunction();
						froggerClient.send(co);
						
						// disable button challenge button and chat
						
					} else {
						// warning that you can't challenge yourself
					}
				}
			}
		});
		
		viewProfileButton.addActionListener(new ActionListener(){
			//override
			public void actionPerformed(ActionEvent ae){
				int index = playerSelectList.getSelectedIndex();
				if (index > -1) {
					String player = playerSelectList.getModel().getElementAt(index);
					// Send player string to server to extract info 
					System.out.println("Viewing profile of: " + player);
					froggerClient.send(new ViewProfileObject(player));
					
					// Testing showProfieScreen with dummy player 
//					PlayerInfo testPlayer = new PlayerInfo("Player1", 10,11,"12:00:00","13:00:00",14,15);
//					showProfileScreen(testPlayer); 
				}
			}
		});
	}
	
	public void challenged(String challenger) {
		JDialog jd = new JDialog();
		ChallengePanel cp = new ChallengePanel(challenger, froggerClient, jd);
		Thread th = new Thread(cp);
		jd.setSize(600,300);
		th.start();
		jd.add(cp);
		jd.setModal(true);
		jd.setLocationRelativeTo(null);
		jd.setMinimumSize(new Dimension(320,240));
		jd.setVisible(true);
	}
	
	
	
	public void disableLobbyFunction() {
		challengeButton.setEnabled(false);
		lobbyChatPanel.disableChatButton();
	}
	
	public void enableLobbyFunction() {
		challengeButton.setEnabled(true);
		lobbyChatPanel.enableChatButton();
	}
	
	public void leaveLobbyScreen() {
		// take
		clearPlayerList();
		enableLobbyFunction();
		lobbyChatPanel.clearText();		
	}

	public void showProfileScreen(PlayerInfo info) {
		JDialog profile = new JDialog(); 
		profile.setTitle(info.getUsername() + "'s Profile");
		profile.setSize(300,400);
		profile.setLocation(600, 100);
		profile.add(new ProfilePanel(info)); 
		profile.setVisible(true);		
	}

	public void setPlayerProfile(PlayerInfo info) {
		this.remove(playerProfile);
		playerProfile = new ProfilePanel(info);
		add(playerProfile, BorderLayout.WEST);
		repaint();
	}
	
	public LobbyChatPanel getLobbyChatPanel() {
		return lobbyChatPanel; 
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		

		g.drawImage(Constants.LILYPADBG.getImage(), 0, 0,getWidth(),getHeight(),this);
	}
}